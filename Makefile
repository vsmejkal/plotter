PATH:=/usr/local/bin/qmake:$(PATH)
PATH:=/usr/local/share/Qt-4.5.0/bin:$(PATH)


all:
		qmake -o src/Makefile src/viz2010.pro
		make -C src

doxygen: 
		doxygen doc/configdoc

run: 
		src/viz2010

clean: 
		rm -rf src/*.o src/viz2010 doc/html src/moc_* src/qrc_* src/Makefile

pack:
		cd .. && tar czf xsychr03.tar.gz xsychr03/
