/**
* @file main.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief řídící funkce
* @details ICP - hlavní funkce řídící chod programu
*/
#include <QApplication>
#include <QTextCodec>
#include <QIcon>
#include "plotter.h"

/**
* @brief main
* @param argc počet parametrů
* @param argv pole parametrů
* @details Vytvoří hlavní widget, nastaví kódování a spustí aplikaci.
*/
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	app.setWindowIcon(QIcon(":/media/plotter.png"));
	QTextCodec::setCodecForTr(QTextCodec::codecForName("iso-8859-2"));
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));

	Plotter *plotter = new Plotter;
	plotter->show();

	return app.exec();
}
