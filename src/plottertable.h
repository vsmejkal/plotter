/**
* @file plottertable.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Tabulka
*/
#ifndef PLOTTERTABLE_H
#define PLOTTERTABLE_H

#include <QTableWidget>
#include <QVector>

using namespace std;

class PlotterDataset;
class Plotter;


/**
 * Tabulka, ktera zobrazuje hodnoty z datovych souboru. Spojuje jednotlivé
 * datasety do sebe a zatříděná data zobrazí v tabulce.
 */
class PlotterTable : public QTableWidget
{
	Q_OBJECT
	
private:
	/// odkaz na okno aplikace
	Plotter *plotter;

public:
	/**
	* @brief Vytvoří widget pro vykreslení tabulky.
	* @param parent rodičovský widget
	*/
	PlotterTable(QWidget *parent = 0);

	/**
	* @brief Zatřídí data z datasetů a překreslí tabulku.
	*/
	void update();

	/**
	* @brief Nastaví odkaz na okno aplikace.
	* @param parent ukazatel na okno aplikace
	*/
	void setRoot(Plotter *parent) {this->plotter = parent;}
};

#endif
