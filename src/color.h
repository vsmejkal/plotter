/**
* @file color.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Správa barev
*/
#ifndef COLOR_H
#define COLOR_H

#include <QColor>


/**
 * @details Třída reprezentující barvu.
 */
class Color : public QColor
{
public:
	/**
	* @brief Generovat barvu podle čísla.
	* @param idx index barvy
	* @details Vygeneruje barvu podle číselného indexu. Zajišťuje, aby měli barvy
	*          s různými indexy co největší rozdílnost.
	*/
	void setFromIndex(int idx);
};

#endif
