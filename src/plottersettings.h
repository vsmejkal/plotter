/**
* @file plottersettings.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul PlotterSettings, zodpovědný za načtení CSV,XML a plain textu
* @details ICP -PlotterSetings - ukládání nastavení programu
*/
#ifndef PLOTTERSETTINGS_H
#define PLOTTERSETTINGS_H

#include <string>
#include <QtXml>
using namespace std;

class Plotter;
/**
* @brief Ukládání nastavení programu
*/
class PlotterSettings 
{
	private:
		//ukazatel na hlavni widget
		Plotter * plotter;
		int width_window;
		int height_window;
		int position_x;
		int position_y;

		//ukladani vlastnosti projektu
		void saveVisibles(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument & document);
		void saveStyle(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument & document);
		void saveColor(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument &document);
		void saveVisibleEl(QDomText &text,QDomDocument & document,bool el);

		//nahravani vlastnosti grafu z XML
		void loadColor(QDomNode &attr,PlotterDataset * new_dataset);
		void loadStyle(QDomNode &attr, PlotterDataset * new_dataset);
		void loadVisibles(QDomNode &attr, PlotterDataset * new_dataset);

		//nastavovani viditelnosti casti grafu
		void setVisibleTitle(QDomNode &attr);
		void setVisibleGridX(QDomNode &attr);
		void setVisibleGridY(QDomNode &attr);
		void setVisibleAxesLabels(QDomNode &attr);
		void setLogarithmicScale(QDomNode &attr);
		void setIsChartVisible(QDomNode &attr);
		void setIsAntialiased(QDomNode &attr);

	public:
		//kontruktor tridy PlotterSettings
		PlotterSettings(Plotter * plotter_instance);
		/**
		* @brief uloží aktuální konfiguraci
		*/
		void saveSettings(string out_file);
		/**
		* @brief nahraje uloženou konfiguraci
		*/
		void loadSettings(string in_file);

};


#endif
