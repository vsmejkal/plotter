/**
* @file plotterchart.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Graf
*/
#ifndef PLOTTERCHART_H
#define PLOTTERCHART_H

#include <QGraphicsView>
#include <QVector>
#include <QPointF>
#include <QPoint>
#include <QMouseEvent>

using namespace std;

class QGraphicsScene;
class QResizeEvent;
class QGraphicsRectItem;
class PlotterDataset;
class Plotter;

/**
 * Kreslici plocha, ktera vykresluje graf. Obsahuje funkce pro nastaveni zdroje
 * dat, moznosti zobrazeni popisku, vykresleni os a mrizky.
 */
class PlotterChart : public QGraphicsView
{
	Q_OBJECT
	
private:
	/// scéna grafu (model)
	QGraphicsScene *scene;
	/// odkaz na okno aplikace
	Plotter *plotter;
	/// rozměry grafu
	QRectF chartRect;
	/// modrý obdelník pro vybírání rozsahu
	QGraphicsRectItem *filterRect;
	/// šířka widgetu bez rámečku
	int size_w;
	/// výška widgetu bez rámečku
	int size_h;
	/// minimální hodnota na ose X
	double xmin;
	/// maximální hodnota na ose X
	double xmax;
	/// minimální hodnota na ose Y
	double ymin;
	/// maximální hodnota na ose Y
	double ymax;
	/// minimální hodnota dat ve filtru
	double filterMin;
	/// maximální hodnota dat ve filtru
	double filterMax;
	/// zda je nastavený filtr
	bool hasFilter;
	/// souřadnice kliknutí při výběru filtru
	QPointF dragPoint;
	
	/**
	* @brief Vykreslí popisky os.
	*/
	void paintAxes();
	
	/**
	* @brief Vykreslí na danou souřadnici řetězec.
	* @param x souřadnice X
	* @param y souřadnice Y
	* @param text řetězec
	*/
	void paintLabel(int x, int y, string text);

	/**
	* @brief Vykreslí na danou souřadnici číslo.
	* @param x souřadnice X
	* @param y souřadnice Y
	* @param num číslo
	*/
	void paintLabel(int x, int y, double num);

	/**
	* @brief Vykreslí daná data do grafu.
	* @param dataset data k vykresleni
	*/
	void paintChart(PlotterDataset *dataset);

	/**
	* @brief Vykreslí název grafu.
	*/
	void paintTitle();

	/**
	* @brief Vykreslí popisky os.
	*/
	void paintAxesLabels();

	/**
	* @brief Převede danou x-ovou souřadnici na index pixelu.
	* @param x souřadnice v grafu
	*/
	double projectX(double x);

	/**
	* @brief Převede danou y-ovou souřadnici na index pixelu.
	* @param y souřadnice v grafu
	*/
	double projectY(double y);

	/**
	* @brief Přepočítá souřadnice myši na souřadnice v grafu.
	* @param point souřadnice myši v grafu
	*/
	QPointF getCoord(QPointF point);

	/**
	* @brief Zobrazí zprávu Žádná data.
	*/
	void noDataMessage();

	/**
	* @brief Událost vyvolaná stisknutím tlačítka myši.
	* @param event objekt události
	*/
	void mousePressEvent(QMouseEvent *event);

	/**
	* @brief Událost vyvolaná pohybem myši přes graf.
	* @param event objekt události
	*/
	void mouseMoveEvent(QMouseEvent *event);

	/**
	* @brief Událost vyvolaná uvolněním tlačítka myši.
	* @param event objekt události
	*/
	void mouseReleaseEvent(QMouseEvent *event);
	
protected:
	/**
	* @brief Událost spuštěná při změně velikosti grafu.
	* @param event objekt události
	*/
	void resizeEvent(QResizeEvent *event);

public slots:
	/**
	* @brief Přiblíží zobrazení.
	*/
	void zoomIn();

	/**
	* @brief Oddálí zobrazení.
	*/
  void zoomOut();

	/**
	* @brief Nastaví přiblížení a filtr na výchozí hodnoty.
	*/
	void zoom100();

	/**
	* @brief Zobrazí okno pro změnu názvu grafu.
	*/
	void changeTitle();

	/**
	* @brief Zobrazí okno pro změnu popisku osy X.
	*/
	void changeXLabel();

	/**
	* @brief Zobrazí okno pro změnu popisku osy Y.
	*/
	void changeYLabel();

public:
	/// název grafu
	string title;
	/// popisek osy X
	string xlabel;
	/// popisek osy Y
	string ylabel;
	/// zda je nastaveno log. měřítko
	bool logarithmicScale;
	/// zda je zobrazena mřížka osy X
	bool xgrid;
	/// zda je zobrazena mřížka osy X
	bool ygrid;
	/// zda je zobrazen název grafu
	bool titleVisible;
	/// zda jsou zobrazeny popisky os
	bool axesLabelsVisible;
	/// zda má graf nějaká data
	bool hasVisibleData;
	/// zda je graf vyhlazený
	bool isAntialiased;

	/**
	* @brief Vytvoří widget pro vykreslování grafu.
	* @param parent rodičovský widget
	*/
	PlotterChart(QWidget *parent = 0);

	/**
	* @brief Zajistí vykreslení celého grafu.
	*/
	void paint();

	/**
	* @brief Překreslí celý graf.
	*/
	void update();

	/**
	* @brief Nastaví měřítko grafu.
	* @param log logaritmické měřítko
	*/
	void setScale(bool logarithmic);

	/**
	* @brief Nastaví ukazatel na okno aplikace.
	*/
	void setRoot(Plotter *parent) { plotter = parent; }

	/**
	* @brief Vrátí scénu (model) grafu.
	*/
	QGraphicsScene *getScene() { return scene; }

	/**
	* @brief Zapne nebo vypne vyhlazování čar grafu.
	* @param state stav vyhlazování
	*/
	void setAntialiasing(bool state);

	/**
	* @brief Zobrazit mřížku osy X.
	*/
	void showXGrid() { xgrid = true; update(); }

	/**
	* @brief Skrýt mřížku osy X.
	*/
	void hideXGrid() { xgrid = false; update(); }

	/**
	* @brief Zobrazit mřížku osy Y.
	*/
	void showYGrid() { ygrid = true; update(); }

	/**
	* @brief Skrýt mřížku osy Y.
	*/
	void hideYGrid() { ygrid = false; update(); }

	/**
	* @brief Zobrazit název grafu.
	*/
	void showTitle() { titleVisible = true; update(); }

	/**
	* @brief Skrýt název grafu.
	*/
	void hideTitle() { titleVisible = false; update(); }

	/**
	* @brief Zobrazit popisky os.
	*/
	void showAxesLabels() { axesLabelsVisible = true; update(); }

	/**
	* @brief Skrýt popisky os.
	*/
	void hideAxesLabels() { axesLabelsVisible = false; update(); }
};

#endif
