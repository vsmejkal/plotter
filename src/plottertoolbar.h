/**
* @file plottertoolbar.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Lišta s menu a tlačítky
*/
#ifndef PLOTTERTOOLBAR_H
#define PLOTTERTOOLBAR_H

#include <QToolBar>
#include "plotter.h"

using namespace std;

class Plotter;


/**
 * @details Horní panel obsahující tlačítka a menu. Obsahuje sloty pro ošetření
 *         všech signálů z obsažených widgetů.
 */
class PlotterToolbar : public QToolBar
{
	Q_OBJECT

private:
	/// odkaz na okno aplikace
	Plotter *plotter;

public slots:
	/**
	* @brief Přepne zobrazení na graf.
	*/
	void toggleChart();

	/**
	* @brief Přepne zobrazení na tabulku.
	*/
	void toggleTable();

	/**
	* @brief Zobrazí okno O Programu.
	*/
	void showAbout();

	/**
	* @brief Zobrazí okno s dokumentací k programu.
	*/
	void showHelp();

	/**
	* @brief Přepne měřítko na lineární.
	*/
	void linearScale() { plotter->chart->setScale(false); }

	/**
	* @brief Přepne měřítko na logaritmické.
	*/
	void logarithmicScale() { plotter->chart->setScale(true); }

	/**
	* @brief Přepne vyhlazování grafu.
	*/
	void toggleAntialiasing();

	/**
	* @brief Přepne zobrazování mřížky osy X.
	*/
	void toggleXGrid();

	/**
	* @brief Přepne zobrazování mřížky osy Y.
	*/
	void toggleYGrid();

	/**
	* @brief Přepne zobrazování názvu grafu.
	*/
	void toggleChartTitle();
	/**
	* @brief Přepne zobrazování popisků os.
	*/
	void toggleAxesLabels();

public:
	/// tlačítko Otevřít
	QToolButton *openButton;
	/// tlačítko Uložit
	QToolButton *saveButton;
	/// tlačítko Zobrazení
	QToolButton *displayButton;
	/// tlačítko Nápověda
	QToolButton *helpButton;
	/// tlačítko Graf/Tabulka
	QToolButton *viewType;
	/// tlačítko přiblížení
	QToolButton *zoomInBtn;
	/// tlačítko oddálení
	QToolButton *zoomOutBtn;
	/// tlačítko výchozí zobrazení
	QToolButton *zoom100Btn;

	/**
	* @brief Vytvoří widget horního menu.
	* @param parent rodičovský widget
	*/
	PlotterToolbar(QWidget *parent = 0);

	/**
	* @brief Nastaví odkaz na okno aplikace.
	*/
	void setRoot(Plotter *parent) { plotter = parent; };
};

#endif
