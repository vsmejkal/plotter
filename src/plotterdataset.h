/**
* @file plotterdataset.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Datový model
*/
#ifndef PLOTTERDATASET_H
#define PLOTTERDATASET_H

#include <vector>
#include <string>
#include <QColor>
#include <Qt>

using namespace std;

class QGraphicsPathItem;

typedef vector<double> TRow;
typedef vector<TRow> TMatrix;


/**
 * @details Soubor dat, ktery se zobrazí v grafu. Obsahuje názvy sloupců, barvy,
 *         styly čar, viditelnost, název grafu a popisky os a vlastní data.
 */
class PlotterDataset
{
private:
	/// matice s daty
	TMatrix data;
	/// minimální hodnota na ose Y
	double y_min;
	/// maximální hodnota na ose Y
	double y_max;

public:
	/// popisky sloupců
	vector<string> labels;
	/// x-ové souřadnice
	vector<double> keys;
	/// barvy čar
	vector<QColor> colors;
	/// styly čar
	vector<Qt::PenStyle> styles;
	/// ukazatele na čáry
	vector<QGraphicsPathItem *> lines;
	/// viditelnost sloupců
	vector<bool> visibles;
	/// název
	string title;

	/**
	* @brief Vytvoří novou instanci datového modelu.
	*/
	PlotterDataset();
	void addLabel(string l) { labels.push_back(l); }
	void addKey(double k) { keys.push_back(k); }

	/**
	* @brief Nastaví položku s indexem řádek,sloupec na hodnotu value.
	* @param row řádek
	* @param col sloupce
	* @param value hodnota
	*/
	void setValue(unsigned int row, unsigned int col, double value);

	/**
	* @brief Nastaví název.
	* @param t název
	*/
	void setTitle(string t) { title = t; }

	/**
	* @brief Získá hodnotu položky s indexem řádek,sloupec.
	* @param row řádek
	* @param col sloupec
	*/
	double getValue(unsigned int row, unsigned int col);

	/**
	* @brief Vrátí název.
	*/
	string getTitle() { return title; }

	/**
	* @brief Vrátí matici dat.
	*/
	TMatrix *getData() { return &data; }

	/**
	* @brief Naplní vektory vychozími hodnotami.
	*/
	void initialize();

	/**
	* @brief Vrátí true, pokud je alespoň jeden sloupec viditelný.
	*/
	bool isVisible();

	/**
	* @brief Získá minimální hodnotu na ose X.
	* @param logarithmic zda má graf logaritmické měřítko
	*/
	double xmin(bool logarithmic = false);

	/**
	* @brief Získá maximální hodnotu na ose X.
	*/
	double xmax();

	/**
	* @brief Získá minimální hodnotu na ose Y.
	*/
	double ymin() { return y_min; }

	/**
	* @brief Získá maximální hodnotu na ose Y.
	*/
	double ymax() { return y_max; }

	/**
	* @brief Vyhledá minimální hodnotu na ose Y.
	*/
	double computeYMin();

	/**
	* @brief Vyhledá maximální hodnotu na ose Y.
	*/
	double computeYMax();

	/**
	* @brief Vrátí počet řádků matice dat.
	*/
	int rows() { return data.size(); }

	/**
	* @brief Vrátí počet sloupců matice dat.
	*/
	int cols() { return (data.size() > 0) ? data[0].size() : 0; }
};

#endif
