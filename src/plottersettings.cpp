/**
* @file plottersettings.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul PlotterSettings, zodpovědný za načtení CSV,XML a plain textu
* @details ICP -PlotterSetings -implementace třídy
*/

#include <string>
#include <iostream>
#include <fstream>
#include "exception.h"
#include <stdlib.h>
#include <QtXml>
#include "plotter.h"
#include "plottersettings.h"
#include "plotterdataset.h"
#include <QColor>
#include <Qt>
#include "dataimport.h"
#include "plottertoolbar.h"
#include <QtGui>
#include "plottertable.h"


#define R 0
#define G 1
#define B 2
#define A 3
using namespace std;

/**
* @brief PlotterSettings konstruktor
*/
PlotterSettings::PlotterSettings(Plotter * plotter_instance)
{
	this->plotter=plotter_instance;
	this->width_window=this->plotter->width();
	this->height_window=this->plotter->height();
	this->position_x=this->plotter->x();
	this->position_y=this->plotter->y();

}


/**
*	@brief saveSettings
*	@param out_file
*/
void PlotterSettings::saveSettings(string out_file)
{
	//otevřeme potřebný soubor
	ofstream ofile;
	ofile.open(out_file.c_str());
	QDomDocument document("plottersettings");
	QFile file(out_file.c_str());
	document.setContent(&file);

	//vytvoření elementu root
	QDomElement root = document.createElement("plottersettings");
	document.appendChild(root);
	//zjistí a uloží rozměry a pozici okna (možno lehce zadat dalši atributy, nutno rozšiřit if)
	QStringList win_attributes=(QStringList() << "wid-win" << "hei-win" << "x-win" \
										               << "y-win" << "xgrid" << "ygrid" << "strtitle" \
														   << "vistitle" << "xlabel" << "ylabel" << "axesLabelsVisible" \
															<< "logarithmicScale" << "isChartVisible" << "isAntialiased");
	//projde atributy z listu win_attributes a zjistí jejich nastavení 
	for(int i=0;i<win_attributes.size();i++)		
	{
		QDomElement attr = document.createElement("attr");
		attr.setAttribute("id",win_attributes[i]);
		QDomText text;
		QString num;
		//šířka okna 
		if(win_attributes[i]=="wid-win")
			text=document.createTextNode(num.setNum(this->width_window));
		//vyska okna
		else if(win_attributes[i]=="hei-win")
			text=document.createTextNode(num.setNum(this->height_window));
		// pozice x
		else if(win_attributes[i]=="x-win")
			text=document.createTextNode(num.setNum(this->position_x));	
		//pozice y
		else if(win_attributes[i]=="y-win")
			text=document.createTextNode(num.setNum(this->position_y));	
		//ulozime viditelnost mrizky X
		else if(win_attributes[i]=="xgrid")
			this->saveVisibleEl(text,document,this->plotter->chart->xgrid);
		//ulozime viditelnost mrizky Y
		else if(win_attributes[i]=="ygrid")
			this->saveVisibleEl(text,document,this->plotter->chart->ygrid);
		//ulozime titulek grafu
		else if(win_attributes[i]=="strtitle")
			text=document.createTextNode(this->plotter->chart->title.c_str());
		//ulozime viditelnost titulku grafu
		else if(win_attributes[i]=="vistitle")
			this->saveVisibleEl(text,document,this->plotter->chart->titleVisible);
		//vyditelnost popisku OS
		else if(win_attributes[i]=="axesLabelsVisible")
			this->saveVisibleEl(text,document,this->plotter->chart->axesLabelsVisible);
		//ulozime popisek osy x
		else if(win_attributes[i]=="xlabel")
			text=document.createTextNode(this->plotter->chart->xlabel.c_str());
		//ulozime popisek osy y
		else if(win_attributes[i]=="ylabel")
			text=document.createTextNode(this->plotter->chart->ylabel.c_str());
		//uklada nastaveni meritka		
		else if(win_attributes[i]=="logarithmicScale")
			this->saveVisibleEl(text,document,this->plotter->chart->logarithmicScale);
		//uklada viditelnosto graf/tabulka
		else if(win_attributes[i]=="isChartVisible")
			this->saveVisibleEl(text,document,this->plotter->isChartVisible);
		//uklada vyhlazenost grafu
		else if(win_attributes[i]=="isAntialiased")
			this->saveVisibleEl(text,document,this->plotter->chart->isAntialiased);
		root.appendChild(attr);
		attr.appendChild(text);
	}

	//vytvorime telo
	QDomElement body = document.createElement("body");
	root.appendChild(body);

	//moje kopie seznamu souboru -> nutne 
	QVector<string> myFilenames =this->plotter->filenames;
	//zjisti informace o grafu
	QStringList graph_attributes = (QStringList()  << "file"  << "color" << "style"<< "visible");
	for(int i=0;i<this->plotter->datas.size();i++)
	{
		PlotterDataset * act_dataset=this->plotter->datas[i];
		//vytvori sekci graph
		QDomElement graph = document.createElement("graph");
		body.appendChild(graph);
		//vytvori novy atribut z graph_attributes
		for(int j=0;j<graph_attributes.size();j++)
		{		
			QDomElement attr = document.createElement("attr");
			attr.setAttribute("id",graph_attributes[j]);
			graph.appendChild(attr);
			QDomText text=document.createTextNode("");
			//zdrojovy soubor pro dany dataset
			if(graph_attributes[j]=="file")
			{			
				text=document.createTextNode(myFilenames.first().c_str());
			   myFilenames.pop_front();
				attr.appendChild(text);
			}
			//zapise posloupnost kanalu barev pro dany dataset
			else if(graph_attributes[j]=="color")
				this->saveColor(act_dataset,attr,document);
			//ulozime styly car
			else if(graph_attributes[j]=="style")
				this->saveStyle(act_dataset,attr,document);	
			//ulozime viditelnost sloupcu	
			else if(graph_attributes[j]=="visible")
				this->saveVisibles(act_dataset,attr,document);

		}

	}
	//zapsani xml documentu do souboru
	QString xml = document.toString();

	ofile << xml.toStdString();
	ofile.close();
	return;
}

/**
* @brief saveVisibleXgrid
* @param text vytvoření textu pro node
* @param document XML document
* @param el bool pravdivostní hodnota zkoumané hodnoty
*/
void PlotterSettings::saveVisibleEl(QDomText &text,QDomDocument & document,bool el)
{
	QString vis("0");
	if(el)
		vis="1";
	text=document.createTextNode(vis);
	return;
}

/**
* @brief saveVisibles
* @param act_dataset aktuální dataset
* @param attr element nesoucí údaje o viditelnych sloupcich
* @param document XML document
*/
void PlotterSettings::saveVisibles(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument & document)
{
	QDomText text=document.createTextNode("");
	for(unsigned int ivisible=0;ivisible<act_dataset->visibles.size();ivisible++)
	{
		QString vis("0");
		if(act_dataset->visibles[ivisible])
			vis="1";
		text=document.createTextNode(vis+",");
		attr.appendChild(text);
	}			

	return;
}
/**
* @brief saveStyle
* @param act_dataset aktuální dataset
* @param attr element nesoucí údaje o stylech
* @param document XML document
*/
void PlotterSettings::saveStyle(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument & document)
{

	//ulozi styly vsech per v danem datasetu
	for(unsigned int istyle=0;istyle < act_dataset->styles.size();istyle++)
	{
		QString num;
		QDomText text=document.createTextNode("");
		text=document.createTextNode(num.setNum(act_dataset->styles[istyle])+",");
		attr.appendChild(text);
	}			
	return;
}
 
/**
* @brief saveColor
* @param act_dataset aktualní dataset
* @param attr element nesoucí údaje o barvách
* @param document XML document
*/
void PlotterSettings::saveColor(PlotterDataset * act_dataset, QDomNode &attr,QDomDocument & document)
{
	//jednotlive kanaly barev
	int  r;
	int  g;
	int  b;
	int  a;
	QDomText text=document.createTextNode("");
	QString r_str,g_str,b_str,a_str;

	//zpracovani vsech definovanych barev pro dany dataset
	for(unsigned int icol=0;icol < act_dataset->colors.size();icol++)
	{
		act_dataset->colors[icol].getRgb(&r,&g,&b,&a);
		text=document.createTextNode(r_str.setNum(r)+","+g_str.setNum(g)+","+b_str.setNum(b)+","+a_str.setNum(a)+";");
		attr.appendChild(text);
	}		
	return;
}

/**
*	@brief loadSettings
*	@param in_file vstupní soubor s konfigurací uloženou ve formátu XML
*/
void PlotterSettings::loadSettings(string in_file)
{
	//otevreme potrebny soubor
	ifstream ifile;
	ifile.open(in_file.c_str());
	if(!ifile.is_open())
		return;
	QDomDocument document("plottersettings");
	QFile file(in_file.c_str());
	document.setContent(&file);

	//nastavime root element
	QDomElement root=document.documentElement();
	QDomNode attr;
	attr=root.firstChild();
	int width=800;
	int height=500;
	int x_position=0;
	int y_position=0;
	//ocekava 12 nastaveni v XML
	for (int i=0;i<14;i++)
	{
		if(!attr.isNull() && attr.toElement().tagName()=="attr")
		{
			QString attribute;
			attribute=attr.toElement().attribute("id");
			if(attribute=="wid-win")
				width=attr.toElement().text().toInt();
			else 	if(attribute=="hei-win")
				height=attr.toElement().text().toInt();
			else 	if(attribute=="x-win")
				x_position=attr.toElement().text().toInt();
			else 	if(attribute=="y-win")
				y_position=attr.toElement().text().toInt();
			else 	if(attribute=="strtitle")
				this->plotter->chart->title=attr.toElement().text().toStdString();
			else 	if(attribute=="vistitle")
				this->setVisibleTitle(attr);
			else 	if(attribute=="xgrid")
				this->setVisibleGridX(attr);
			else 	if(attribute=="ygrid")
				this->setVisibleGridY(attr);
			else 	if(attribute=="axesLabelsVisible")
				this->setVisibleAxesLabels(attr);
			else 	if(attribute=="xlabel")
				this->plotter->chart->xlabel=attr.toElement().text().toStdString();
			else 	if(attribute=="ylabel")
				this->plotter->chart->ylabel=attr.toElement().text().toStdString();
			else 	if(attribute=="logarithmicScale")
				this->setLogarithmicScale(attr);
			else 	if(attribute=="isChartVisible")
				this->setIsChartVisible(attr);
			else 	if(attribute=="isAntialiased")
				this->setIsAntialiased(attr);


			else
				throw FormatError("Chybne atributy");

		}
		else
			throw FormatError("Chybny format");
		//prejde na dalsi node
		attr=attr.nextSibling();
	}
	//nastavi sirku, vysku, pozici
	this->plotter->setGeometry(x_position,y_position,width,height);

	//najdeme element  body
	QDomNodeList bodys = document.elementsByTagName("body");
	QDomNode body;
	if(!bodys.isEmpty())
		body = bodys.at(0);
	else
		throw FormatError("Chybi element body");

	//projdeme grafy
	QDomNode graph=body.firstChild();
	while(!graph.isNull() && graph.toElement().tagName()=="graph")
	{
		//vytvorime novy dataset
		PlotterDataset * new_dataset=NULL;
		
		//a v nich jednotlive atributy
		//vzdy vkladame do noveho datasetu, krome souboru
		attr=graph.firstChild();
		while(!attr.isNull() && attr.toElement().tagName()=="attr")
		{
			QString attribute_text=attr.toElement().attribute("id");
			// pridame do plotter filenames na konec novy soubor			
			if(attribute_text=="file")
			{
				this->plotter->filenames.push_back(attr.toElement().text().toStdString());				
				new_dataset= DataImport(attr.toElement().text().toStdString()).getData();			
				//new_dataset->initialize();
			}
			//nahraje barvy
			else if(attribute_text=="color")
				this->loadColor(attr,new_dataset);
			//prirazeni stylu caram
			else if(attribute_text=="style")
				this->loadStyle(attr,new_dataset);
			//nacteni viditelnosti car			
			else if(attribute_text=="visible")
				this->loadVisibles(attr,new_dataset);
			
			attr=attr.nextSibling();
		}
		//vlozime na konec mnoziny datasetu novy dataset
		this->plotter->datas.push_back(new_dataset);
	
		graph=graph.nextSibling();
	}	

	ifile.close();
	this->plotter->update();
	return;
}


/*
* @brief isAntialiased
* @param attr uzel xml, který obsahuje informace o vyhlazenosti grafu
*/
void PlotterSettings::setIsAntialiased(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
	this->plotter->chart->setAntialiasing(bol);
   this->plotter->toolbar->displayButton->menu()->actions()[4]->setChecked(bol);
}

/*
* @brief setIsChartVisible
* @param attr uzel xml, který obsahuje informace o zobrazení tabulka/graf
*/
void PlotterSettings::setIsChartVisible(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();

	this->plotter->isChartVisible=attr.toElement().text().toInt();

	if(bol)
	{
		this->plotter->toolbar->toggleChart();
		plotter->table->hide();	
	}
	else
	{
		this->plotter->toolbar->toggleTable();
		plotter->chart->hide();
	}		
return;
}
/*
* @brief setLogarithmicScale
* @param attr uzel xml, který obsahuje informace o zvoleném měřítku
*/
void PlotterSettings::setLogarithmicScale(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
//	this->plotter->chart->setScale(bol);
	if(bol)
	{
		this->plotter->toolbar->logarithmicScale();	

		this->plotter->toolbar->displayButton->menu()->actions()[6]->toggle();
		//this->plotter->toolbar->displayButton->menu()->actions()[5]->setChecked(true);		
	}
	else
		this->plotter->toolbar->linearScale();	
	return;
}

/*
* @brief setVisibleAxesLabels
* @param attr uzel xml, který obsahuje informace o viditelnosti popisků os
*/
void PlotterSettings::setVisibleAxesLabels(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
	if(bol)
 		this->plotter->chart->showAxesLabels();
	else
		this->plotter->chart->hideAxesLabels();
   this->plotter->toolbar->displayButton->menu()->actions()[1]->setChecked(bol);
}
/*
* @brief setVisibleGridY
* @param attr uzel xml, který obsahuje informace o viditelnosti mřížky Y
*/
void PlotterSettings::setVisibleGridY(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
	if(bol)
		this->plotter->chart->showYGrid();
	else
		this->plotter->chart->hideYGrid();
   this->plotter->toolbar->displayButton->menu()->actions()[3]->setChecked(bol);
}
/*
* @brief setVisibleGridX
* @param attr uzel xml, který obsahuje informace o viditelnosti mřížky X
*/
void PlotterSettings::setVisibleGridX(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
	if(bol)
		this->plotter->chart->showXGrid();
	else
		this->plotter->chart->hideXGrid();
   this->plotter->toolbar->displayButton->menu()->actions()[2]->setChecked(bol);
}
/*
* @brief setVisibleTitle
* @param attr uzel xml, který obsahuje informace o viditelnosti Titulku
*/
void PlotterSettings::setVisibleTitle(QDomNode &attr)
{
	int bol=attr.toElement().text().toInt();
	if(bol)
		this->plotter->chart->showTitle();
	else
		this->plotter->chart->hideTitle();
   this->plotter->toolbar->displayButton->menu()->actions()[0]->setChecked(bol);
}
/*
* @brief loadVisibles
* @param attr uzel xml, který obsahuje informace o viditelnosti čar
* @param new_dataset aktualní dataset (typ PlotterDataset *)
*/
void PlotterSettings::loadVisibles(QDomNode &attr, PlotterDataset * new_dataset)
{
	QString visibles_text=attr.toElement().text();
	int itext=visibles_text.size() -1;
	QStringList visibles_list=visibles_text.remove(itext,1).split(",");

	//nahrajeme visibles
	for(int i=0;i<visibles_list.size();i++)
	{
		
		if(visibles_list[i]=="1")
			new_dataset->visibles.push_back(true);
		else
			new_dataset->visibles.push_back(false);			
	}
	return;
}
/*
* @brief loadStyle
* @param attr uzel xml, který obsahuje informace o stylech
* @param new_dataset aktualní dataset (typ PlotterDataset *)
*/
void PlotterSettings::loadStyle(QDomNode &attr, PlotterDataset * new_dataset)
{				
	QString styles_text=attr.toElement().text();
	int itext=styles_text.size() -1;
	QStringList styles_list=styles_text.remove(itext,1).split(",");
	for(int i=0;i<styles_list.size();i++)
	{
		Qt::PenStyle pen_style;
		switch(styles_list[i].toInt())
		{
			case 0: pen_style = Qt::NoPen; break;
			case 1: pen_style = Qt::SolidLine; break;
			case 2: pen_style = Qt::DashLine; break;
			case 3: pen_style = Qt::DotLine; break;
			case 4: pen_style = Qt::DashDotLine; break;
			case 5: pen_style = Qt::DashDotDotLine; break;
			case 6: pen_style = Qt::CustomDashLine; break;
		}
	
		new_dataset->styles.push_back(pen_style);
	}
	return;
}
/*
* @brief loadColor
* @param attr uzel xml, který obsahuje informace a barvach
* @param new_dataset aktualní dataset (typ PlotterDataset *)
*/
void PlotterSettings::loadColor(QDomNode &attr, PlotterDataset * new_dataset)
{
	//vytvorime si novy list, kde budou hodnoty po 4rech
	QString colors_text=attr.toElement().text();
	int itext=colors_text.size() -1;
	QStringList colors_list=colors_text.remove(itext,1).split(";");

	//zapise jednotlive barvy postupne do datasetu
	int r,g,b,a;
	r=g=b=a=0;

	//hledani a zapsani barev			
	for(int i=0;i<colors_list.size();i++)
	{
		QStringList rgb_list = colors_list[i].split(",");
		QColor color;
		color.setRgb(rgb_list[R].toInt(),rgb_list[G].toInt(),rgb_list[B].toInt(),rgb_list[A].toInt());
		new_dataset->colors.push_back(color);
	}
	return;
}
