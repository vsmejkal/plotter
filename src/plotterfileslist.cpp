/**
* @file plotterfilelist.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Levý panel otevřených souborů
*/
#include <QtGui>
#include "plotter.h"
#include "plotterfileslist.h"
#include "plotterdataset.h"

using namespace std;


/**
* @brief Vytvoří widget pro seznam otevřených souborů.
* @param parent rodičovský widget
*/
PlotterFilesList::PlotterFilesList(QWidget *parent) : QTreeWidget(parent)
{
	updating = false;
	setHeaderLabel(tr("Otevřené soubory"));
	setContextMenuPolicy(Qt::DefaultContextMenu);
	connect(this, SIGNAL(itemChanged(QTreeWidgetItem *, int)),
	        this, SLOT(checkHandler(QTreeWidgetItem *, int)));
}


/**
* @brief Překreslí seznam otevřených souborů.
*/
void PlotterFilesList::update()
{
	updating = true;
	clear();
	QTreeWidgetItem *itemFile, *itemCol;
	QVector<string> filenames = plotter->filenames;
	QVector<PlotterDataset *> datas = plotter->datas;
	int filesCount = filenames.size();
	
	// projit vsechny soubory
	for (int i = 0; i < filesCount; i++) {
		PlotterDataset *ds = datas[i];
		int columnsCount = ds->cols();
		QFileInfo file(filenames[i].c_str());

		itemFile = new QTreeWidgetItem(this);
		itemFile->setFlags(itemFile->flags()|Qt::ItemIsUserCheckable);
		itemFile->setText(0, file.fileName());
		itemFile->setToolTip(0, file.filePath());
		itemFile->setExpanded(true);

		if (ds->isVisible())
			itemFile->setCheckState(0, Qt::Checked);
		else
			itemFile->setCheckState(0, Qt::Unchecked);

		// projit vsechny sloupce
		for (int j = 0; j < columnsCount; j++) {
			QPixmap square(32, 32);
			square.fill(ds->colors[j]);
			itemCol = new QTreeWidgetItem(itemFile);
			itemCol->setFlags(itemCol->flags()|Qt::ItemIsUserCheckable);
			itemCol->setText(0, ds->labels[j + 1].c_str());
			itemCol->setIcon(0, QIcon(square));
			
			if (ds->visibles[j])
				itemCol->setCheckState(0, Qt::Checked);
			else
				itemCol->setCheckState(0, Qt::Unchecked);
		}
	}

	updating = false;
}


/**
* @brief Zobrazí nebo skryje datové sloupce.
* @param item položka v seznamu
* @param c index sloupce
* @details Vyvolá se po kliknutí na zaškrtávací políčko v seznamu otevřených
*          souborů.
*/
void PlotterFilesList::checkHandler(QTreeWidgetItem *item, int c)
{
	if (updating)
		return;

	// soubor
	if (item->parent() == 0) {
		Qt::CheckState state = item->checkState(c);

		for (int i = 0; i < item->childCount(); i++)
			if (item->child(i)->checkState(c) != state)
				item->child(i)->setCheckState(c, state);
	}

	// sloupec v souboru
	else {
		int dataset_idx = indexOfTopLevelItem(item->parent());
		int column_idx = item->parent()->indexOfChild(item);

		if (item->checkState(c) == Qt::Checked)
			plotter->showColumn(dataset_idx, column_idx);
		else
			plotter->hideColumn(dataset_idx, column_idx);
	}
}


/**
* @brief Zobrazí kontextovou nabídku.
* @param event objekt události
* @details Vyvolá se po kliknutí druhým tlačítkem myši na seznam otevřených
*          souborů
*/
void PlotterFilesList::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu menu;
	QTreeWidgetItem *item = itemAt(event->pos());
	if (item == 0)
		return;

	// soubor
	if (item->parent() == 0) {
		menu.addAction(QIcon(":/media/dialog_close.png"), tr("Odstranit"),
		               this, SLOT(removeDataset()));
	}

	// sloupec v souboru
	else {
		int ds_idx = indexOfTopLevelItem(item->parent());
		int col_idx = item->parent()->indexOfChild(item);
		Qt::PenStyle style = plotter->datas[ds_idx]->styles[col_idx];

		menu.addAction(QIcon(":/media/color_line.png"), tr("Barva"),
		               this, SLOT(changeItemColor()));

		QMenu *strokeMenu = menu.addMenu(QIcon(":/media/pencil.png"), tr("Čára"));
		QActionGroup *group = new QActionGroup(strokeMenu);
		QAction *act1, *act2, *act3;

		act1 = strokeMenu->addAction(tr("Plná"), this, SLOT(setSolidLine()));
		act1->setCheckable(true);
		group->addAction(act1);

		act2 = strokeMenu->addAction(tr("Čárkovaná"), this, SLOT(setDashLine()));
		act2->setCheckable(true);
		group->addAction(act2);

		act3 = strokeMenu->addAction(tr("Čerchovaná"), this, SLOT(setDashDotLine()));
		act3->setCheckable(true);
		group->addAction(act3);

		if (style == Qt::SolidLine)
			act1->setChecked(true);
		else if (style == Qt::DashLine)
			act2->setChecked(true);
		else
			act3->setChecked(true);
	}
	
	menu.exec(QCursor::pos());
}


/**
* @brief Zobrazí paletu barev a změní barvu čáry grafu.
*/
void PlotterFilesList::changeItemColor()
{
	QColor col = QColorDialog::getColor();
	QList<QTreeWidgetItem *> items = selectedItems();

	// stisknuto Cancel nebo neni nic vybrano
	if (not col.isValid() or items.size() == 0)
		return;

	// zmenit barvu polozky
	QPixmap square(32, 32);
	square.fill(col);
	items[0]->setIcon(0, QIcon(square));

	int file_idx = indexOfTopLevelItem(items[0]->parent());
	int item_idx = items[0]->parent()->indexOfChild(items[0]);
	plotter->setColor(file_idx, item_idx, col);
}


/**
* @brief Změní styl čáry grafu.
* @param style styl čáry
*/
void PlotterFilesList::changeLineStyle(Qt::PenStyle style)
{
	QList<QTreeWidgetItem *> items = selectedItems();
	if (items.size() == 0)
		return;

	int dataset_idx = indexOfTopLevelItem(items[0]->parent());
	int column_idx = items[0]->parent()->indexOfChild(items[0]);
	plotter->setLineStyle(dataset_idx, column_idx, style);
}


/**
* @brief Odstraní vybraný soubor a uvolní paměť.
*/
void PlotterFilesList::removeDataset()
{
	QList<QTreeWidgetItem *> items = selectedItems();
	if (items.size() == 0)
		return;

	int dataset_idx = indexOfTopLevelItem(items[0]);
	removeItemWidget(items[0], 0);
	plotter->removeDataset(dataset_idx);
}
