/**
* @file glwidget.cpp
* @author VojtÄch Smejkal (xsmejk07) , TomĂĄĹĄ Sychra (xsychr03)
* @date 6.5.2010
* @brief GLWidget na vykreslovĂĄnĂ­ grafu
*/
#include <QtGui>
#include <QtOpenGL>
#include "glwidget.h"


/**
*	@brief NastavĂ­ vyhlazovĂĄnĂ­ pomocĂ­ sample buffers
*/	
void GLWidget::initializeGL()
{
	glEnable(GL_MULTISAMPLE);
	GLint bufs;
	GLint samples;
	glGetIntegerv(GL_SAMPLE_BUFFERS, &bufs);
	glGetIntegerv(GL_SAMPLES, &samples);
}

