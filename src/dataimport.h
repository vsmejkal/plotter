/**
* @file dataimport.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul DataImport, zodpovědný za načtení CSV,XML a plain textu
* @details ICP -Třída DataImport
*/

#ifndef DATAIMPORT_H
#define DATAIMPORT_H

#include <string>
#include <vector>
#include <fstream>
#include "plotterdataset.h"
#include <sstream>
#include <iostream>
#include <stdlib.h>
#include "exception.h"
 #include <QTextStream>

using namespace std;

/**
*	@brief DataImport 
*	@details Třída zajištující načtení CSV,XML,plain text.
*/
class DataImport 
{
	private:
		//zpracovavany vstupni soubor
		ifstream input_file;
		string file_string;
		//typ souboru
		int text_input;
		int csv_input;
		int xml_input;
		//privatni metody
		/**
		*	@brief parseLine
		*	@param str_line řádek k rozparsování
		*	@param data objekt pro uchování informací načtených ze souboru
		*	@param row slouží jako index při  vkládání do objektu typu PlotterDataset
		*	@details parsování řádku s hodnotami grafu
		*/
		void parseLine(string str_line,int row,PlotterDataset *data);
		/**
		*	@brief readLine
		*	@param line nově načtený řádek
		*	@param input_file vstupní soubor s daty
		*	@details načte řádek ze vstupního souboru
		*/
		void readLine(string &line,ifstream &input_file);
		/**
		*	@brief parseLabel 
		*	@param str_line řádek pro rozparsování
		*	@param data objekt pro uchování informací z načteného souboru
		*	@details načte popisku sloupců u plain textu
		*/
		void parseLabel(string line,PlotterDataset *data);
		/**
		*	@brief getText
		*	@param data  objekt pro uchování informací načtených ze souboru
		*	@details řídící metoda pro načítání plain textu
		*/
		PlotterDataset * getText(PlotterDataset * data);
		/**
		*	@brief getCsv
		*	@param data  objekt pro uchování informací načtených ze souboru
		*	@details načte csv soubor do paměti
		*/
		PlotterDataset * getCsv(PlotterDataset * data);
		/**
		*	@brief getXML
		*	@param data  objekt pro uchování informací načtených ze souboru
		*	@details načte XML soubor do paměti
		*/
		PlotterDataset * getXML(PlotterDataset * data);
	public:
		/**
		* @brief DataImport 
		* @param str_file vstupní soubor
		* @details Konstruktor třídy DataImport.
		*/	
		DataImport(string str_file);
		
		/**
		*	@brief getData
		*	@details načte a vrátí odkaz na načtená data uložená v objektu typy PlotterDataset
		*/
		PlotterDataset * getData();



};

#endif
