/**
* @file plottertable.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Tabulka
*/
#include <QtGui>
#include <cmath>
#include "plotter.h"
#include "plottertable.h"
#include "plotterdataset.h"
#include "dataimport.h"

using namespace std;


/**
* @brief Vytvoří widget pro vykreslení tabulky.
* @param parent rodičovský widget
*/
PlotterTable::PlotterTable(QWidget *parent) : QTableWidget(parent)
{
	horizontalHeader()->setResizeMode(QHeaderView::Custom);
	setEditTriggers(QTableWidget::NoEditTriggers);
}


/**
* @brief Zatřídí data z datasetů a překreslí tabulku.
*/
void PlotterTable::update()
{
	clear();
	hide();
	QStringList hlabels;
	QStringList vlabels;
	
	double key;
	bool hasVisibleData = false;
	int cols_cnt = 0;
	TMatrix data;
	TRow new_row;
	vector<string>::iterator lab;
	QVector<PlotterDataset *>::iterator ds;

	// spojit datasety do matice
	for (ds = plotter->datas.begin(); ds < plotter->datas.end(); ds++) {
		if (not (*ds)->isVisible())
			continue;

		hasVisibleData = true;
		
		// ziskat labely
		for (unsigned int c = 1; c < (*ds)->labels.size(); c++)
			if ((*ds)->visibles[c - 1])
				hlabels << QString((*ds)->labels[c].c_str());

		int rows = (*ds)->rows();
		int cols = (*ds)->cols();
		unsigned int i = 0;

		// zatridit radky
		for (int r = 0; r < rows; r++) {
			key = (*ds)->keys[r];
			
			// klic je vetsi
			while (i < data.size() && key > data[i][0]) {
				for (int c = 0; c < cols; c++)
					if ((*ds)->visibles[c])
						data[i].push_back(NAN);
				i++;
			}

			// klice se rovnaji
			if (i < data.size() && data[i][0] == key) {
				for (int c = 0; c < cols; c++)
					if ((*ds)->visibles[c])
						data[i].push_back((*ds)->getValue(r, c));
				i++;
			}

			// klic nebyl nalezen
			else {
				new_row.clear();
				new_row.push_back(key);
				for (int c = 0; c < cols_cnt; c++)
					new_row.push_back(NAN);
				for (int c = 0; c < cols; c++)
					if ((*ds)->visibles[c])
						new_row.push_back((*ds)->getValue(r, c));
				data.insert(data.begin() + i, new_row);
				i++;
			}
		}

		for (int c = 0; c < cols; c++)
			if ((*ds)->visibles[c])
				cols_cnt++;
	}

	// zadna viditelna data
	if (not hasVisibleData) {
		hide();
		plotter->chart->update();
		plotter->chart->show();
		return;
	}


	int rows = data.size();
	int cols = (rows == 0) ? 0 : data.front().size() - 1;
	int r = 0;
	int c = 0;
	TMatrix::iterator row;
	TRow::iterator col;
	ostringstream ss;
	QTableWidgetItem *cell;

	ss.precision(4);
	setRowCount(rows);
	setColumnCount(cols);

	// naplnit tabulku
	for (row = data.begin(); row < data.end(); row++, r++, c = 0)
		for (col = row->begin(); col < row->end(); col++, c++) {
			// bunka neni prazdna
			if (not isnan(*col))
				ss << *col;
			
			// prvni sloupec je klic
			if (c == 0)
				vlabels << QString(ss.str().c_str());
			else {
				cell = new QTableWidgetItem(QString(ss.str().c_str()));
				setItem(r, c - 1, cell);
			}
			ss.str("");
		}

	setHorizontalHeaderLabels(hlabels);
	setVerticalHeaderLabels(vlabels);

	if (not plotter->isChartVisible)
		show();
}
