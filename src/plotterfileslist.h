/**
* @file plotterfilelist.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Levý panel otevřených souborů
*/
#ifndef PLOTTERFILESLIST_H
#define PLOTTERFILESLIST_H

#include <Qt>
#include <QTreeWidget>
#include <QVector>

using namespace std;

class Plotter;
class PlotterDataset;


/**
 * Levy panel obsahujici strom otevrenych soubor s datovymi sadami.
 */
class PlotterFilesList : public QTreeWidget
{
	Q_OBJECT

private:
	/// odkaz na okno aplikace
	Plotter *plotter;
	/// zda probíhá generování seznamu
	bool updating;

public slots:
	/**
	* @brief Změní styl čáry grafu.
	* @param style styl čáry
	*/
	void changeLineStyle(Qt::PenStyle style);

	/**
	* @brief Zobrazí nebo skryje datové sloupce.
	* @param item položka v seznamu
	* @param c index sloupce
	* @details Vyvolá se po kliknutí na zaškrtávací políčko v seznamu otevřených
	*          souborů.
	*/
	void checkHandler(QTreeWidgetItem *item, int column);

	/**
	* @brief Zobrazí kontextovou nabídku.
	* @param event objekt události
	* @details Vyvolá se po kliknutí druhým tlačítkem myši na seznam otevřených
	*          souborů
	*/
	void contextMenuEvent(QContextMenuEvent * event);

	/**
	* @brief Odstraní vybraný soubor a uvolní paměť.
	*/
	void removeDataset();

	/**
	* @brief Zobrazí paletu barev a změní barvu čáry grafu.
	*/
	void changeItemColor();

	/**
	* @brief Nastaví styl čáry na plnou.
	*/
	void setSolidLine() { changeLineStyle(Qt::SolidLine); }

	/**
	* @brief Nastaví styl čáry na čárkovanou.
	*/
	void setDashLine() { changeLineStyle(Qt::DashLine); }

	/**
	* @brief Nastaví styl čáry na čerchovanou.
	*/
	void setDashDotLine() { changeLineStyle(Qt::DashDotLine); }

public:
	/**
	* @brief Vytvoří widget pro seznam otevřených souborů.
	* @param parent rodičovský widget
	*/
	PlotterFilesList(QWidget *parent = 0);

	/**
	* @brief Překreslí seznam otevřených souborů.
	*/
	void update();

	/**
	* @brief Nastaví odkaz na okno aplikace.
	*/
	void setRoot(Plotter *parent) {this->plotter = parent;}
};

#endif
