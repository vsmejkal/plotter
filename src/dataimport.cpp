/**
* @file dataimport.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul DataImport, zodpovědný za načtení CSV,XML a plain textu
* @details ICP -Implementace třídy DataImport
*/
#include "dataimport.h"
#include "plotterdataset.h"
#include <ctype.h>
#include "exception.h"
#include <clocale>
#include <string>
#include <QDomDocument>
#include <QtXml>
using namespace std;



/**
* @brief DataImport 
* @param str_file vstupní soubor
* @details Konstruktor třídy DataImport.
*/	
DataImport::DataImport(string str_file)
{
	this->text_input=0;
	this->csv_input=0;
	this->xml_input=0;

	this->file_string=str_file;
	this->input_file.open(str_file.c_str());
	if(!(this->input_file.is_open()))
	{
		
		throw IOError("Soubor nelze otevrit");
	}	
	string extension;
	string::reverse_iterator rit;
	//hleda koncovku souboru	
	for(rit=str_file.rbegin(); rit < str_file.rend();rit++)
	{
		if(*rit!='.')
			extension=*rit+extension;
		else break;
	}

	//bezpecne rozpozna csv a xml, nezhoduje-li se , zkusi plain text
	if(extension=="xml")
		this->xml_input=1;
	else if(extension=="csv")
		this->csv_input=1;
	else 
		this->text_input=1;


}

/**
*	@brief parseLabel 
*	@param str_line řádek pro rozparsování
*	@param data objekt pro uchování informací z načteného souboru
*	@details načte popisku sloupců u plain textu
*/
void DataImport::parseLabel(string str_line,PlotterDataset *data)
{
	string word;
	//pripravime buffer	
	istringstream is;
	is.str(str_line);
	//postupne vyzvedavame honoty z bufferu
	while((is >> word)!=NULL)
		data->addLabel(word);
}

/**
*	@brief parseLine
*	@param str_line řádek k rozparsování
*	@param data objekt pro uchování informací načtených ze souboru
*	@param row slouží jako index při  vkládání do objektu typu PlotterDataset
*	@details parsování řádku s hodnotami grafu
*/
void DataImport::parseLine(string str_line,int row,PlotterDataset *data) 
{
	// strtod bude nacitat desetinne tecky
	setlocale(LC_NUMERIC, "C");
	
	string word;
	int column=0;
	//pripravime buffer	
	istringstream is;
	is.str(str_line);
	char *error_pos;
	
	//postupne vyzvedavame honoty z bufferu
	while((is >> word)!=NULL)
	{
		double num=strtod(word.c_str(),&error_pos);
		//parsuje klic
		if(column==0)
		{
			/* - prevadi klic na cislo	
				- ostreju aby se nemichaly string a cisla v klicich*/
			if(*error_pos==0)
				data->addKey(num);
			else
				throw FormatError("Spatny format souboru");
		}
		/* - parsuje hodnoty X Y
			- mohou byt pouze cisla */
		else
		{
			if(*error_pos==0)
				data->setValue(row,column-1,num);
			else
				throw FormatError("Spatny format souboru");
		}
		++column;
	}
}
/**
*	@brief readLine
*	@param line nově načtený řádek
*	@param input_file vstupní soubor s daty
*	@details načte řádek ze vstupního souboru
*/
void DataImport::readLine(string &line,ifstream &input_file)
{
	char c;
	line.clear();
	//cte radek dokud nenarazi na znak konce radku
	while(((c=input_file.get())!='\n') && !input_file.eof())
		line+=c;
}

/**
*	@brief getText
*	@param data  objekt pro uchování informací načtených ze souboru
*	@details řídící metoda pro načítání plain textu
*/
PlotterDataset * DataImport::getText(PlotterDataset * data)
{
	
	string curr_line="";
	string prev_line="";
	string label="";
	string meta_info="";
	string word="";
	int meta_tag=0;
	//nacte uvodni metatagy, pocet sloupci a legendu
	while(meta_tag==0 && !(this->input_file.eof()))
	{
		this->readLine(curr_line,this->input_file);
		//aktualni radek nezacina # -> podezreni na data
		if(curr_line[0]!='#')
		{	
			/*zjistujeme jak na tom byl predchozi radek 
			zacinal-li # jedna se o label*/
			if(prev_line[0]=='#')
			{
				this->parseLabel(prev_line.substr(1),data);
				meta_tag=1;
			}			
			else
				throw FormatError("Spatny format souboru");
		}
		/*jinak pridame prechozi radku do meta_informaci
		a soucasnou si zapamatujeme*/
		else
		{
			//musime hlidat, jelikoz prev_line muze byt v prvnim kroku nedefinovana
			if(prev_line.size()!=0)
			{	
				//zachovava formatvani
				prev_line.push_back('\n');
				meta_info+=prev_line.substr(1);
			}			
			prev_line=curr_line;
		}
	}
	data->setTitle(meta_info);
	//nacte samotna data
	int row=0;
	while(!(this->input_file.eof()))
	{
		//parsuje radek	
		this->parseLine(curr_line,row,data);
		//nacita dalsi radek		
		this->readLine(curr_line,this->input_file);
		++row;
	}

	return data;
}

/**
*	@brief getXML
*	@param data  objekt pro uchování informací načtených ze souboru
*	@details načte XML soubor do paměti
*/
PlotterDataset * DataImport::getXML(PlotterDataset * data)
{
	// strtod bude nacitat desetinne tecky
	setlocale(LC_NUMERIC, "C");

	QDomDocument document("DataGraph");
	QFile file(this->file_string.c_str());
	//zda nutno dodelat osetreni chyb
	document.setContent(&file);
	
	//nastavime root element
	QDomElement root=document.documentElement();
	QDomNode head;
	
	head=root.firstChild();
	int max_col=0;
	//nacte head
	if(!head.isNull() && head.toElement().tagName()=="head")
	{
		//zjistime pocet sloupcu, ktery budeme v body vyzadovat
	 	
		QDomNode col;
		//pokusi se dostat na uroven sloupce		
		if(!(col=head.firstChild().firstChild()).isNull())
		{	
			//jeden sloupec je zarucen
			if(col.toElement().tagName()=="title")
				data->setTitle(col.toElement().text().toStdString());
			else
			{
				data->addLabel(col.toElement().text().toStdString());
				++max_col;
			}
			//zpracuje ostatni sloupce			
			while(!(col=col.nextSibling()).isNull())
			{
				data->addLabel(col.toElement().text().toStdString());
				++max_col;
			}
		}
		else
			throw FormatError("Chyba v sekci head");
	
	}		
	else
		throw FormatError("Chybi sekce head");

	//nacte body
	QDomNode body=head.nextSibling();
	if(!body.isNull() && body.toElement().tagName()=="body")
	{
		QDomNode row=body.firstChild();
		if(row.isNull())
			throw FormatError("Chyba vstupniho souboru");
		int n_row=0;		
		while(!row.isNull())
		{	
			QDomNode col=row.firstChild();
			if(col.isNull())
				throw FormatError("Chyba vstupniho souboru");
			int n_col=0;		
			double num=0;		
			//projdeme sloupce daneho radku
			while(!col.isNull())
			{
				stringstream cols;
				cols <<  col.toElement().text().toStdString() ;
				cols >> num;
				//kontrola prevodu
				if(!cols.eof())
					throw FormatError("Nevalidni format cisla");
				
				if(n_col==0)
					data->addKey(num);
				else
					data->setValue(n_row,n_col-1,num);	
				
				++n_col;
				col=col.nextSibling();
			}
			++n_row;
			row=row.nextSibling();
			//kontrolujeme,zda-li nam sedi sloupce
			if(max_col!=n_col)
				throw FormatError("Chybny pocet sloupcu");
		
		}

	}
	else
		throw FormatError("Chybi sekce body");

	return data;
}
/**
*	@brief getCsv
*	@param data  objekt pro uchování informací načtených ze souboru
*	@details načte csv soubor do paměti
*/
PlotterDataset * DataImport::getCsv(PlotterDataset * data)
{
	//void DataImport::readLine(string &line,ifstream &input_file)
	string line="";	
	QStringList col;
	int n_row=0;
	int max_col=0;
	string control("");

	while(!(this->input_file.eof()))
	{
		//nacteme novy radek
		this->readLine(line,this->input_file);
		//rozparsujeme radek
		QString qline(line.c_str());
		col=qline.split(",");
		
		if(!col.isEmpty())	
		{	
			//popisek grafu
			data->addLabel(col.takeAt(0).toStdString());
			//ukladani dat ve sloupcich
			int n_col=0;
			double num=0;
			while(!col.isEmpty())
			{				
				num=0;
				stringstream cols;
				control=col.front().toStdString() ;
				cols << control;
				col.pop_front();
				cols >> num;
				if(!cols.eof() || control=="" )
					throw FormatError("Nevalidni format cisla");
				if(n_row==0)
				{
					data->addKey(num);
					++max_col;			
				}	
				else				
				{
					data->setValue(n_col,n_row-1,num);
					++n_col;			
				}				

			}
			
			//kontroluje sloupce		
			if(!(this->input_file.eof()) && n_row!=0 && n_col!=max_col)
			{
				throw FormatError("Chybny pocet sloupcu");
			}

		}

		++n_row;
	}
  return data;
}


/**
*	@brief getData
*	@details načte a vrátí odkaz na načtená data uložená v objektu typy PlotterDataset
*/
PlotterDataset * DataImport::getData()
{
	//vytvorime si novy objekt pro ukladani dat	
	PlotterDataset *data= new PlotterDataset;	
	if(this->text_input)	
	{
		data=this->getText(data);
	}	
	else if(this->csv_input)
	{	
		data=this->getCsv(data);	
	}	
	else if(this->xml_input)
	{	
		data=this->getXML(data);
	}	

	return data;
}



