/**
* @file plotterdataset.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Datový model
*/
#include "plotterdataset.h"
#include <Qt>
#include <vector>
#include <algorithm>
#include <limits>
#include "exception.h"

using namespace std;


/**
* @brief Vytvoří novou instanci datového modelu.
*/
PlotterDataset::PlotterDataset()
{
	y_min = y_max = 0.0;
}


/**
* @brief Naplní vektory vychozími hodnotami.
*/
void PlotterDataset::initialize()
{
	visibles.clear();
	styles.clear();
	int size = cols();

	for (int i = 0; i < size; i++) {
		visibles.push_back(true);
		styles.push_back(Qt::SolidLine);
	}
}


/**
* @brief Vrátí true, pokud je alespoň jeden sloupec viditelný.
*/
bool PlotterDataset::isVisible()
{
	for (unsigned int i = 0; i < visibles.size(); i++)
		if (visibles[i])
			return true;

	return false;
}


/**
* @brief Nastaví položku s indexem řádek,sloupec na hodnotu value.
* @param row řádek
* @param col sloupce
* @param value hodnota
*/
void PlotterDataset::setValue(unsigned int row, unsigned int col, double value)
{
	// rozstahnout vektor radku
	if (this->data.size() <= row)
		this->data.resize(row + 1);
		
	// roztahnout vektor sloupce
	if (this->data[row].size() <= col)
		this->data[row].resize(col + 1);
	
	this->data[row][col] = value;

	// proverit minimum a maximum
	y_min = (value < y_min) ? value : y_min;
	y_max = (value > y_max) ? value : y_max;
}


/**
* @brief Získá hodnotu položky s indexem řádek,sloupec.
* @param row řádek
* @param col sloupec
*/
double PlotterDataset::getValue(unsigned int row, unsigned int col)
{
	if (data.size() <= row || data[row].size() <= col)
		throw RangeError("");

	return data[row][col];
}


/**
* @brief Získá minimální hodnotu na ose X.
* @param logarithmic zda má graf logaritmické měřítko
*/
double PlotterDataset::xmin(bool logarithmic)
{
	if (keys.empty())
		return 0.0;

	if (logarithmic) {
		for (unsigned int i = 0; i < keys.size(); i++)
			if (keys[i] > 0)
				return keys[i];

		return 0.0;
	}
	
	return keys.front();
}


/**
* @brief Získá maximální hodnotu na ose X.
*/
double PlotterDataset::xmax()
{
	if (keys.empty())
		return 0.0;
	else
		return keys.back();
}


/**
* @brief Vyhledá minimální hodnotu na ose Y.
*/
double PlotterDataset::computeYMin()
{
	double y_min = numeric_limits<double>::max();
	double row_min;

	if (data.empty())
		y_min = 0.0;
	else
		for (TMatrix::iterator row = data.begin(); row < data.end(); row++) {
			row_min = *min_element(row->begin(), row->end());
			y_min = (row_min < y_min) ? row_min : y_min;
		} 

	return y_min;
}


/**
* @brief Vyhledá maximální hodnotu na ose Y.
*/
double PlotterDataset::computeYMax()
{
	double y_max = -numeric_limits<double>::max();
	double row_max;

	if (data.empty())
		y_max = 0.0;
	else
		for (TMatrix::iterator row = data.begin(); row < data.end(); row++) {
			row_max = *max_element(row->begin(), row->end());
			y_max = (row_max > y_max) ? row_max : y_max;
		}

	return y_max;
}

