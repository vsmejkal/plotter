/**
* @file plotterchart.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Graf
*/
#include <QtGui>
#include <QtAlgorithms>
#include "plotter.h"
#include "plotterchart.h"
#include "plotterdataset.h"
#include "plottertoolbar.h"
#include "glwidget.h"
#include "dataimport.h"
#include "color.h"
#include <sstream>
#include <limits>
#include <cmath>


/**
* @brief Vytvoří widget pro vykreslování grafu.
* @param parent rodičovský widget
*/
PlotterChart::PlotterChart(QWidget *parent) : QGraphicsView(parent)
{
	hasVisibleData = false;
	hasFilter = false;
	logarithmicScale = false;
	titleVisible = false;
	axesLabelsVisible = false;
	xgrid = true;
	ygrid = true;
	dragPoint.setX(-1);
	dragPoint.setY(-1);
	scene = new QGraphicsScene;
	scene->setBackgroundBrush(Qt::white);
	setScene(scene);
	setAntialiasing(true);
	setViewport(new GLWidget);

	// nastavit rozmery sceny
	int frame_w = frameWidth();
	size_w = width() - 2 * frame_w;
	size_h = height() - 2 * frame_w;

}


/**
* @brief Překreslí celý graf.
*/
void PlotterChart::update()
{
	// odstranit vsechny objekty
	qDeleteAll(scene->items());
	
	QVector<PlotterDataset *> datas = plotter->datas;
	QVector<PlotterDataset *>::iterator ds;
	hasVisibleData = false;

	for (int i = 0; i < datas.size(); i++)
		if (datas[i]->isVisible()) {
			hasVisibleData = true;
			break;
		}

	if (not hasVisibleData) {
		noDataMessage();
		return;
	}

	xmin = numeric_limits<double>::max();
	xmax = -numeric_limits<double>::max();
	ymin = numeric_limits<double>::max();
	ymax = -numeric_limits<double>::max();

	// najit minima a maxima
	for (ds = datas.begin(); ds < datas.end(); ds++) {
		if (not (*ds)->isVisible())
			continue;

		double new_xmin = (*ds)->xmin(logarithmicScale);
		double new_xmax = (*ds)->xmax();
		double new_ymin = (*ds)->ymin();
		double new_ymax = (*ds)->ymax();

		if (new_xmin < xmin)
			xmin = new_xmin;
		if (new_xmax > xmax)
			xmax = new_xmax;
		if (new_ymin < ymin)
			ymin = new_ymin;
		if (new_ymax > ymax)
			ymax = new_ymax;
	}

	if (hasFilter) {
		xmin = filterMin;
		xmax = filterMax;
	}

	if (logarithmicScale && xmax <= 0)
		noDataMessage();
	else
		paint();
}


/**
* @brief Zobrazí zprávu Žádná data.
*/
void PlotterChart::noDataMessage()
{
	QString str(tr("Žádná data"));
	QFont font("Arial", 32);
	zoom100();

	QGraphicsTextItem *item = scene->addText(str, font);
	int w = item->boundingRect().width();
	int h = item->boundingRect().height();
	item->setPos((size_w - w) / 2, (size_h - h) / 2);
	item->setDefaultTextColor(QColor("#ddd"));
}


/**
* @brief Událost spuštěná při změně velikosti grafu.
* @param event objekt události
*/
void PlotterChart::resizeEvent(QResizeEvent *event)
{
	// nastavit rozmery sceny
	int frame_w = frameWidth();
	size_w = width() - 2 * frame_w;
	size_h = height() - 2 * frame_w;
	scene->setSceneRect(0, 0, size_w , size_h);
	centerOn(size_w / 2, size_h / 2);
	
	// premalovat vse
	update();
}


/**
* @brief Zajistí vykreslení celého grafu.
*/
void PlotterChart::paint()
{
	int MARGIN_T = 30;
	int MARGIN_B = 30;
	int MARGIN_L = 45;
	int MARGIN_R = 20;

	if (titleVisible)
		MARGIN_T += 15;
	if (axesLabelsVisible)
		MARGIN_B += 15;
	if (axesLabelsVisible)
		MARGIN_L += 20;
		
	chartRect.setRect(MARGIN_L,
	                  MARGIN_T,
	                  size_w - MARGIN_L - MARGIN_R,
	                  size_h - MARGIN_T - MARGIN_B);
	QVector<PlotterDataset *>::iterator ds;

	// vykreslit vsechny datasety
	for (ds = plotter->datas.begin(); ds < plotter->datas.end(); ds++) {

		if ((*ds)->isVisible())
			paintChart(*ds);
	}
	
	// vykreslit souradny system
	paintAxes();

	// vykreslit nazev grafu
	if (titleVisible)
		paintTitle();

	// vykreslit popisky os
	if (axesLabelsVisible)
		paintAxesLabels();
}


/**
* @brief Vykreslí název grafu.
*/
void PlotterChart::paintTitle() {
	QString text = QString::fromStdString(title);
	QFont font("Arial", 12);

	if (text.isEmpty() && plotter->datas.size() > 0)
		text = QString::fromStdString(plotter->datas[0]->title);

	QGraphicsTextItem *item = scene->addText(text, font);
	int w = item->boundingRect().width();
	item->setPos(chartRect.left() + (chartRect.width() - w) / 2, 10);
	item->setZValue(5);
}


/**
* @brief Vykreslí popisky os.
*/
void PlotterChart::paintAxesLabels() {
	QString xstr = QString::fromStdString(xlabel);
	QString ystr = QString::fromStdString(ylabel);
	QFont font("Arial", 8);
	font.setBold(true);
	int w;

	if (xstr.isEmpty() && plotter->datas.size() > 0)
		if (plotter->datas[0]->labels.size() > 0)
			xstr = QString::fromStdString(plotter->datas[0]->labels[0]);

	if (ystr.isEmpty() && plotter->datas.size() > 0)
		if (plotter->datas[0]->labels.size() > 1)
			ystr = QString::fromStdString(plotter->datas[0]->labels[1]);

	if (not xstr.isEmpty()) {
		QGraphicsTextItem *xlab = scene->addText(xstr, font);
		w = xlab->boundingRect().width();
		xlab->setPos(chartRect.left() + (chartRect.width() - w) / 2,
			           chartRect.bottom() + 18);
		xlab->setZValue(5);
	}

	if (not ystr.isEmpty()) {
		QGraphicsTextItem *ylab = scene->addText(ystr, font);
		w = ylab->boundingRect().width();
		ylab->rotate(-90);
		ylab->translate(-w, 0);
		ylab->setPos(5, chartRect.top() + (chartRect.height() - w) / 2);
		ylab->setZValue(5);
	}
}


/**
* @brief Vykreslí na danou souřadnici řetězec.
* @param x souřadnice X
* @param y souřadnice Y
* @param text řetězec
*/
void PlotterChart::paintLabel(int x, int y, string text)
{
	QString str(text.c_str());
	QFont font("Arial", 8);

	QGraphicsTextItem *item = scene->addText(str, font);
	int w = item->boundingRect().width();
	int h = item->boundingRect().height();
	item->setPos(x - w / 2, y - h / 2 + 1);
	item->setZValue(4);
}


/**
* @brief Vykreslí na danou souřadnici číslo.
* @param x souřadnice X
* @param y souřadnice Y
* @param num číslo
*/
void PlotterChart::paintLabel(int x, int y, double num)
{
	ostringstream ss;
	ss.precision((num > 1 && num < 1000) ? 3 : 2);
	ss << num;
	paintLabel(x, y, ss.str());
}


/**
* @brief Vykreslí osy grafu a mřížku.
*/
void PlotterChart::paintAxes()
{
	const double GRID_X = 50.0;
	const double GRID_Y = 30.0;
	const int DASH_SIZE = 4;
	const int TEXT_MOVE_X = 12;
	const int TEXT_MOVE_Y = -24;

	qreal x1, y1, x2, y2;
	chartRect.getCoords(&x1, &y1, &x2, &y2);
	QPen grid_pen(QColor("#ccc"));

	// bile obdelniky po stranach grafu (orezavani grafu)
	QPen pen(QColor("#fff"));
	QBrush brush(QColor("#fff"));
	QGraphicsItem *rightRect = scene->addRect(
		chartRect.right() + 1, 0,
		size_w - chartRect.right(), size_h,
		pen, brush);
	QGraphicsItem *leftRect = scene->addRect(
		0, 0,
		chartRect.left(), size_h,
		pen, brush);
	rightRect->setZValue(3);
	leftRect->setZValue(3);

	// vykreslit osy
	scene->addLine(x1, y1 - 1, x1, y2)->setZValue(4);
	scene->addLine(x1, y2, x2 + 1, y2)->setZValue(4);

	int width = chartRect.width();
	int xparts = floor(width / GRID_X);
	double xsize = width / (double) xparts;
	double xval = xmin;
	double xstep = (xmax - xval) / (double) xparts;
	double logmin = log10(xmin);
	double logmax = log10(xmax);
	double logrange = logmax - logmin;
	
	// vykreslit osu x
	for (int i = 0; i <= xparts; i++) {
		double x = x1 + i * xsize;
		if (logarithmicScale)
			xval = pow(10, logmin + (i / (double) xparts) * logrange);
		// carka u popisku
		scene->addLine(x, y2, x, y2 + DASH_SIZE)->setZValue(4);
		// popisek
		paintLabel(x, y2 + TEXT_MOVE_X, xval);
		// cara mrizky
		if (xgrid && i > 0)
			scene->addLine(x, y2, x, y1 - 1, grid_pen)->setZValue(1);

		xval += xstep;
	}
	
	int height = chartRect.height();
	int yparts = floor(height / GRID_Y);
	double ysize = height / (double) yparts;
	double yval = ymin;
	double ystep = (ymax - yval) / (double) yparts;

	// vykreslit osu y
	for (int i = 0; i <= yparts; i++) {
		double y = y2 - i * ysize;
		// carka u popisku
		scene->addLine(x1 - DASH_SIZE, y, x1, y)->setZValue(4);
		// popisek
		paintLabel(x1 + TEXT_MOVE_Y, y, yval);
		// cara mrizky
		if (ygrid && i > 0)
			scene->addLine(x1, y, x2 + 1, y, grid_pen)->setZValue(1);
		yval += ystep;
	}
}


/**
* @brief Převede danou x-ovou souřadnici na index pixelu.
* @param x souřadnice v grafu
*/
double PlotterChart::projectX(double x)
{
	qreal x1, y1, x2, y2;
	chartRect.getCoords(&x1, &y1, &x2, &y2);
	int w = chartRect.width();

	if (logarithmicScale) {
		if (x <= 0)
			return x;
		else
			return x1 + w * log10(x / xmin) / log10(xmax / xmin);
	}
	else
		return x1 + w * (x - xmin) / (xmax - xmin);
}


/**
* @brief Převede danou y-ovou souřadnici na index pixelu.
* @param y souřadnice v grafu
*/
double PlotterChart::projectY(double y)
{
	qreal x1, y1, x2, y2;
	chartRect.getCoords(&x1, &y1, &x2, &y2);
	int h = chartRect.height();

	return y2 - h * (y - ymin) / (ymax - ymin);
}


/**
* @brief Přepočítá souřadnice myši na souřadnice v grafu.
* @param point souřadnice myši v grafu
*/
QPointF PlotterChart::getCoord(QPointF point)
{
	double xpos = (point.x() - chartRect.x()) / (double) chartRect.width();
	double ypos = (chartRect.bottom() - point.y()) / (double) chartRect.height();
	double xcoor;
	double ycoor = ymin + ypos * (ymax - ymin);

	if (logarithmicScale) {
		double logmin = log10(xmin);
		double logmax = log10(xmax);
		double logrange = logmax - logmin;
		xcoor = pow(10, logmin + xpos * logrange);
	}
	else {
		xcoor = xmin + xpos * (xmax - xmin);
	}

	return QPointF(xcoor, ycoor);
}


/**
* @brief Vykreslí daná data do grafu.
* @param dataset data k vykresleni
*/
void PlotterChart::paintChart(PlotterDataset *dataset)
{
	// zadna data
	if (dataset->rows() == 0)
		return;
	
	int rows = dataset->rows();
	int cols = dataset->cols();
	TMatrix data = *dataset->getData();
	TRow keys = dataset->keys;
	int first = (logarithmicScale) ? -1 : 0;  // prvni X

	// vypocitat souradnice X
	for (int r = 0; r < rows; r++) {
		if (first == -1 && keys[r] > 0)
			first = r;
		keys[r] = projectX(keys[r]);
	}

	// data nelze na log. meritku zobrazit
	if (first == -1)
		return;
	
	QPen pen;
	QGraphicsPathItem *line;
	dataset->lines.clear();

	// vykreslit graf
	for (int c = 0; c < cols; c++) {
		QPainterPath path(QPointF(keys[first], projectY(data[first][c])));
		pen.setStyle(dataset->styles[c]);
		pen.setColor(dataset->colors[c]);

		for (int r = first + 1; r < rows; r++) {
			path.lineTo(keys[r], projectY(data[r][c]));
		}

		line = scene->addPath(path, pen);
		line->setZValue(2);
		dataset->lines.push_back(line);
		
		if (not dataset->visibles[c])
			line->hide();
	}
}


/**
* @brief Nastaví měřítko grafu.
* @param log logaritmické měřítko
*/
void PlotterChart::setScale(bool log)
{
	logarithmicScale = log;
	update();
}


/**
* @brief Zapne nebo vypne vyhlazování čar grafu.
* @param state stav vyhlazování
*/
void PlotterChart::setAntialiasing(bool state)
{
	if (state) {
		isAntialiased = true;
		setRenderHints(QPainter::Antialiasing |
		               QPainter::SmoothPixmapTransform |
		               QPainter::TextAntialiasing);
		
		QGLFormat glf = QGLFormat::defaultFormat();
    glf.setSampleBuffers(true);
    glf.setSamples(4);
    QGLFormat::setDefaultFormat(glf);
	}
	else {
		isAntialiased = false;
		setRenderHints(QPainter::TextAntialiasing);
	}
}


/**
* @brief Přiblíží zobrazení.
*/
void PlotterChart::zoomIn()
{
	if (not hasVisibleData)
		return;

	scale(1.25, 1.25);
}


/**
* @brief Oddálí zobrazení.
*/
void PlotterChart::zoomOut()
{
	if (not hasVisibleData)
		return;

	scale(1 / 1.25, 1 / 1.25);

	if (matrix().m11() < 1 || matrix().m22() < 1)
		resetMatrix();
}


/**
* @brief Nastaví přiblížení a filtr na výchozí hodnoty.
*/
void PlotterChart::zoom100()
{
	resetMatrix();

	if (hasFilter) {
		hasFilter = false;
		update();
	}
}


/**
* @brief Zobrazí okno pro změnu názvu grafu.
*/
void PlotterChart::changeTitle()
{
	QString text = QInputDialog::getText(this,
		tr("Název grafu"),
		tr("Zadejte název grafu:"));
	
	if (not text.isEmpty()) {
		title = text.toStdString();
		titleVisible = true;
		plotter->toolbar->displayButton->menu()->actions()[0]->setChecked(true);
		update();
	}
}


/**
* @brief Zobrazí okno pro změnu popisku osy X.
*/
void PlotterChart::changeXLabel()
{
	QString text = QInputDialog::getText(this,
		tr("Popisek osy x"),
		tr("Zadejte popisek osy x:"));
	
	if (not text.isEmpty()) {
		xlabel = text.toStdString();
		axesLabelsVisible = true;
		plotter->toolbar->displayButton->menu()->actions()[1]->setChecked(true);
		update();
	}
}


/**
* @brief Zobrazí okno pro změnu popisku osy Y.
*/
void PlotterChart::changeYLabel()
{
	QString text = QInputDialog::getText(this,
		tr("Popisek osy y"),
		tr("Zadejte popisek osy y:"));

	if (not text.isEmpty()) {
		ylabel = text.toStdString();
		axesLabelsVisible = true;
		plotter->toolbar->displayButton->menu()->actions()[1]->setChecked(true);
		update();
	}
}


/**
* @brief Událost vyvolaná stisknutím tlačítka myši.
* @param event objekt události
*/
void PlotterChart::mousePressEvent(QMouseEvent *event)
{
	if (not hasVisibleData)
		return;

	if (event->buttons() == Qt::LeftButton) {
		dragPoint.setX(event->x() - viewportTransform().dx());
		dragPoint = matrix().inverted().map(dragPoint);
		dragPoint.setX(max(chartRect.left(), min(dragPoint.x(), chartRect.right())));
		dragPoint.setY(chartRect.top());

		filterRect = scene->addRect(QRectF());
		filterRect->setZValue(100);
		filterRect->setPen(QPen(QColor(0, 0, 0, 0)));
		filterRect->setBrush(QBrush(QColor(0, 130, 255, 60)));
	}
	else if (event->buttons() == Qt::RightButton && hasVisibleData) {
		QMenu menu;
		menu.addAction(tr("Změnit název grafu"), this, SLOT(changeTitle()));
		menu.addAction(tr("Změnit popisek X"), this, SLOT(changeXLabel()));
		menu.addAction(tr("Změnit popisek Y"), this, SLOT(changeYLabel()));
		menu.exec(QCursor::pos());
	}
}


/**
* @brief Událost vyvolaná pohybem myši přes graf.
* @param event objekt události
*/
void PlotterChart::mouseMoveEvent(QMouseEvent *event)
{
	if (event->buttons() == Qt::LeftButton && dragPoint.x() >= 0) {
		QPointF dropPoint;
		dropPoint.setX(event->x() - viewportTransform().dx());
		dropPoint = matrix().inverted().map(dropPoint);
		dropPoint.setX(max(chartRect.left(), min(dropPoint.x(), chartRect.right())));
		dropPoint.setY(chartRect.bottom());

		QRectF rect(dragPoint, dropPoint);
		filterRect->setRect(rect);
	}
}


/**
* @brief Událost vyvolaná uvolněním tlačítka myši.
* @param event objekt události
*/
void PlotterChart::mouseReleaseEvent(QMouseEvent *event)
{
	if (dragPoint.x() >= 0) {
		filterMin = getCoord(filterRect->rect().topLeft()).x();
		filterMax = getCoord(filterRect->rect().topRight()).x();
		if (filterMin > filterMax)
			swap(filterMin, filterMax);
		hasFilter = filterMax - filterMin > 0;

		update();
		dragPoint.setX(-1);
		dragPoint.setY(-1);
	}
}

