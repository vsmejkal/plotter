/**
* @file glwidget.h
* @author VojtÄch Smejkal (xsmejk07) , TomĂĄĹĄ Sychra (xsychr03)
* @date 6.5.2010
* @brief GLWidget na vykreslovĂĄnĂ­ grafu.
*/
#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>


/**
* @details Widget vykreslujĂ­cĂ­ graf. SlouĹžĂ­ k akcelerovanĂŠmu vykreslovĂĄnĂ­ pomocĂ­
*         knihovny OpenGL.
*/
class GLWidget : public QGLWidget
{
public:
	/**
	*	@brief NastavĂ­ vyhlazovĂĄnĂ­ pomocĂ­ sample buffers
	*/	
	void initializeGL();
};

#endif
