/**
* @file plottertoolbar.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Lišta s menu a tlačítky
*/
#include <QtGui>
#include <QWebView>
#include "plotter.h"
#include "plottertoolbar.h"
#include "plottertable.h"
#include "plotterchart.h"

using namespace std;


/**
* @brief Vytvoří widget horního menu.
* @param parent rodičovský widget
*/
PlotterToolbar::PlotterToolbar(QWidget *parent) : QToolBar(parent)
{
	QAction *act;
	setIconSize(QSize(16, 16));
	setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	// tlacitko Otevrit
	openButton = new QToolButton;
	openButton->setText(tr("Otevřít"));
	openButton->setIcon(QIcon(":/media/add.png"));
	openButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	addWidget(openButton);

	// tlacitko Ulozit
	saveButton = new QToolButton;
	saveButton->setText(tr("Uložit"));
	saveButton->setIcon(QIcon(":/media/filesave.png"));
	saveButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	addWidget(saveButton);

	// menu Zobrazit
	QMenu *displayMenu = new QMenu;
	QActionGroup *measureGroup = new QActionGroup(displayMenu);

	act = displayMenu->addAction(tr("Název grafu"), this, SLOT(toggleChartTitle()));
	act->setCheckable(true);

	act = displayMenu->addAction(tr("Popisky os"), this, SLOT(toggleAxesLabels()));
	act->setCheckable(true);

	act = displayMenu->addAction(tr("Mřížka osy X"), this, SLOT(toggleXGrid()));
	act->setCheckable(true);
	act->setChecked(true);

	act = displayMenu->addAction(tr("Mřížka osy Y"), this, SLOT(toggleYGrid()));
	act->setCheckable(true);
	act->setChecked(true);

	act = displayMenu->addAction(tr("Vyhlazování"), this, SLOT(toggleAntialiasing()));
	act->setCheckable(true);
	act->setChecked(true);
	displayMenu->addSeparator();

	act = displayMenu->addAction(tr("Lineární měřítko"), this, SLOT(linearScale()));
	act->setCheckable(true);
	act->setChecked(true);
	measureGroup->addAction(act);

	act = displayMenu->addAction(tr("Logaritmické měřítko"), this, SLOT(logarithmicScale()));
	act->setCheckable(true);
	measureGroup->addAction(act);

	// tlacitko Zobrazit
	displayButton = new QToolButton;
	displayButton->setText(tr("Zobrazit"));
	displayButton->setIcon(QIcon(":/media/display.png"));
	displayButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	displayButton->setMenu(displayMenu);
	displayButton->setPopupMode(QToolButton::InstantPopup);
	addWidget(displayButton);

	// menu Napoveda
	QMenu *helpMenu = new QMenu;
	helpMenu->addAction(QIcon(":/media/help_contents.png"), tr("Dokumentace"),
	                    this, SLOT(showHelp()));
	helpMenu->addAction(QIcon(":/media/help_about.png"), tr("O programu"),
	                    this, SLOT(showAbout()));

	// tlacitko Napoveda
	helpButton = new QToolButton;
	helpButton->setText(tr("Nápověda"));
	helpButton->setIcon(QIcon(":/media/help.png"));
	helpButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	helpButton->setMenu(helpMenu);
	helpButton->setPopupMode(QToolButton::InstantPopup);
	addWidget(helpButton);

	// spacer
	QWidget *toolbarSpacer = new QWidget;
	toolbarSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	addWidget(toolbarSpacer);

	// menu typ zobrazeni
	QMenu *viewMenu = new QMenu;
	viewMenu->addAction(QIcon(":/media/chart.png"), tr("XY graf"),
	                    this, SLOT(toggleChart()));
	viewMenu->addAction(QIcon(":/media/table.png"), tr("Tabulka"),
	                    this, SLOT(toggleTable()));

	// typ zobrazeni
	viewType = new QToolButton;
	viewType->setText(tr("XY graf"));
	viewType->setIcon(QIcon(":/media/chart.png"));
	viewType->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	viewType->setMenu(viewMenu);
	viewType->setPopupMode(QToolButton::InstantPopup);
	addWidget(viewType);

	// zoom in
	zoomInBtn = new QToolButton;
	zoomInBtn->setIcon(QIcon(":/media/zoom_in.png"));
	addWidget(zoomInBtn);

	// zoom in
	zoomOutBtn = new QToolButton;
	zoomOutBtn->setIcon(QIcon(":/media/zoom_out.png"));
	addWidget(zoomOutBtn);

	// zoom 100%
	zoom100Btn = new QToolButton;
	zoom100Btn->setIcon(QIcon(":/media/zoom_original.png"));
	addWidget(zoom100Btn);
}


/**
* @brief Přepne zobrazení na graf.
*/
void PlotterToolbar::toggleChart()
{
	plotter->table->hide();
	plotter->chart->show();

	plotter->isChartVisible = true;
	viewType->setIcon(QIcon(":/media/chart.png"));
	viewType->setText("XY Graf");
	plotter->chart->update();
}


/**
* @brief Přepne zobrazení na tabulku.
*/
void PlotterToolbar::toggleTable()
{
	if (plotter->chart->hasVisibleData) {
		plotter->chart->hide();
		plotter->table->show();
	}
	
	plotter->isChartVisible = false;
	viewType->setIcon(QIcon(":/media/table.png"));
	viewType->setText("Tabulka");
	plotter->table->update();
}


/**
* @brief Přepne zobrazování názvu grafu.
*/
void PlotterToolbar::toggleChartTitle()
{
	bool state = displayButton->menu()->actions()[0]->isChecked();
	if (state)
		plotter->chart->showTitle();
	else
		plotter->chart->hideTitle();
}


/**
* @brief Přepne zobrazování popisků os.
*/
void PlotterToolbar::toggleAxesLabels()
{
	bool state = displayButton->menu()->actions()[1]->isChecked();
	if (state)	
		plotter->chart->showAxesLabels();
	else
		plotter->chart->hideAxesLabels();
}


/**
* @brief Přepne zobrazování mřížky osy X.
*/
void PlotterToolbar::toggleXGrid()
{
	bool state = displayButton->menu()->actions()[2]->isChecked();
	if (state)	
		plotter->chart->showXGrid();
	else
		plotter->chart->hideXGrid();
}


/**
* @brief Přepne zobrazování mřížky osy Y.
*/
void PlotterToolbar::toggleYGrid()
{
	bool state = displayButton->menu()->actions()[3]->isChecked();
	if (state)	
		plotter->chart->showYGrid();
	else
		plotter->chart->hideYGrid();

}


/**
* @brief Přepne vyhlazování grafu.
*/
void PlotterToolbar::toggleAntialiasing()
{
	bool state = displayButton->menu()->actions()[4]->isChecked();
	plotter->chart->setAntialiasing(state);
}


/**
* @brief Zobrazí okno O Programu.
*/
void PlotterToolbar::showAbout()
{
	QWidget *win = new QWidget;
	win->setWindowIcon(QIcon(":/media/help.png"));
	win->setWindowFlags(Qt::Dialog);
	win->setWindowTitle(tr("O programu"));
	win->resize(350, 370);

	QWidget *icon = new QWidget(win);
	icon->resize(350, 160);
	icon->setStyleSheet(
		"background-image:url(:/media/plotter.png);"
		"background-repeat: no-repeat;"
		"background-position:center center;"
	);
	
	QLabel *title = new QLabel(tr("Plotter"), win);
	title->resize(350, 80);
	title->move(0, 145);
	title->setAlignment(Qt::AlignHCenter);
	title->setFont(QFont("Arial", 18, QFont::Bold));

	QLabel *subtitle = new QLabel(tr("Vizualizační software"), win);
	subtitle->resize(350, 20);
	subtitle->move(0, 178);
	subtitle->setAlignment(Qt::AlignHCenter);

	QLabel *authors = new QLabel(win);
	authors->resize(350, 100);
	authors->move(0, 220);
	authors->setAlignment(Qt::AlignHCenter);
	authors->setTextFormat(Qt::RichText);
	authors->setText(tr(
		"<b>Autoři</b><br/>"
		"Vojtěch Smejkal, <a href='mailto:xsmejk07@stud.fit.vutbr.cz'>xsmejk07@stud.fit.vutbr.cz</a><br/>"
		"Tomáš Sychra, <a href='mailto:xsychr03@stud.fit.vutbr.cz'>xsychr03@stud.fit.vutbr.cz</a><br/><br/>"
		"Fakulta informačních technologií VUT"
	));

	QPushButton *okButton = new QPushButton(tr("Zavřít"), win);
	okButton->resize(80, 25);
	okButton->move(140, 330);
	connect(okButton, SIGNAL(clicked()), win, SLOT(close()));

	win->show();
}


/**
* @brief Zobrazí okno s dokumentací k programu.
*/
void PlotterToolbar::showHelp()
{
	QString base = QApplication::applicationDirPath();
	QWebView *helpWin = new QWebView;
	helpWin->setWindowTitle(tr("Nápověda k aplikaci Plotter"));
	helpWin->load(base + "/../help/index.html");
	helpWin->show();
}

