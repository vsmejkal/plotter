/**
* @file color.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Správa barev
*/
#include "color.h"


/**
* @brief Generovat barvu podle čísla.
* @param idx index barvy
* @details Vygeneruje barvu podle číselného indexu. Zajišťuje, aby měli barvy
*          s různými indexy co největší rozdílnost.
*/
void Color::setFromIndex(int idx)
{
	int hue = 0;

	if (idx > 0) {
		int parts = 2;

		// zjistit pocet dilku
		while (idx >= parts)
			parts *= 2;

		// vypocitat odstin
		hue = (1.0 / parts) * (1 + 2 * idx - parts) * 360;
	}

	this->setHsv(hue, 255, 255);
}

