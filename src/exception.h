/**
* @file exception.h
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul Exception, obsluha výjimek
* @details ICP - deklarace tříd výjimek
*/

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>
#include <iostream>
#include <exception>

using namespace std;


/**
*	@details Předpis pro ostatní výjimky.
*/
class Exception
{
	private:
		string  str_exception;
	public:
		Exception(string msg);
		string msg() { return this->str_exception; };
};


/**
*	@details Chyba práce se soubory.
*/
class IOError : public Exception {
	public:
		IOError(string msg): Exception(msg){};
};


/**
*	@details Špatný formát souboru.
*/
class FormatError : public Exception {
	public:
		FormatError(string msg): Exception(msg){};
};


/**
*	@details Index mimo rozsah pole.
*/
class RangeError : public Exception
{
	public:
		RangeError(string msg): Exception(msg) {};
};

#endif
