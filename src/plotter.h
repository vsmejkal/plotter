/**
* @file plotter.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief hlavní okno aplikace
*/
#ifndef PLOTTER_H
#define PLOTTER_H

#include <QWidget>
#include <QMatrix>
#include <QVector>
#include "plotterchart.h"

class QToolButton;
class PlotterToolbar;
class PlotterChart;
class PlotterTable;
class PlotterFilesList;
class PlotterSettings;


/**
 * @details Hlavní třída obsahující celé uživatelské rozhraní. Stará se
 *         o vykreslení všech částí a ošetření uživatelských událostí.
 */
class Plotter : public QWidget
{
	Q_OBJECT

public:
	/// názvy souborů
	QVector<string> filenames;
	/// datasety
	QVector<PlotterDataset *> datas;
	/// ukazatel na graf
	PlotterChart *chart;
	/// ukazatel na tabulku
	PlotterTable *table;
	/// ukazatel na horní toolbar
	PlotterToolbar *toolbar;
	/// ukazatel na seznam otevřených souborů
	PlotterFilesList *openedFiles;
	/// zda je viditelný graf nebo tabulka
	bool isChartVisible;

	/**
	* @brief Vytvoří hlavní okno programu s celým jeho obsahem.
	* @param parent rodičovský widget
	*/
	Plotter(QWidget *parent = 0);

	/**
	* @brief Přesune okno aplikace na střed obrazovky.
	*/
	void centerWindow();

	/**
	* @brief Změní barvu čáry v grafu.
	* @param ds_idx index datasetu
	* @param column_idx index datového sloupce
	* @param color barva
	*/
	void setColor(int file_idx, int item_idx, QColor color);

	/**
	* @brief Změní styl čáry v grafu.
	* @param ds_idx index datasetu
	* @param column_idx index datového sloupce
	* @param style styl čáry
	*/
	void setLineStyle(int dataset_idx, int column_idx, Qt::PenStyle style);

	/**
	* @brief Zobrazí datový sloupec a překreslí pohledy.
	* @param ds_idx index datasetu
	* @param column_idx index datového sloupce
	*/
	void showColumn(int ds_idx, int column_idx);

	/**
	* @brief Schová datový sloupec a překreslí pohledy.
	* @param ds_idx index datasetu
	* @param column_idx index datového sloupce
	*/
	void hideColumn(int ds_idx, int column_idx);

	/**
	* @brief Odstraní dataset z paměti a obnoví pohledy.
	* @param ds_idx index datasetu
	*/
	void removeDataset(int ds_idx);

public slots:
	/**
	* @brief Zobrazí dialog pro otevření souboru a přidá ho.
	*/
	void openFile();

	/**
	* @brief Zobrazí dialog pro uložení a uloží obrázek grafu.
	*/
	void saveChart();

	/**
	* @brief Událost vyvolaná při ukončení aplikace. Uloží nastavení.
	* @param event objekt události
	*/
	void closeEvent(QCloseEvent *event);

private:
	/**
	* @brief Nastaví vzhled aplikace podle CSS souboru.
	*/
	void applyStyle();

	/**
	* @brief Znovunastavit barvy čar.
	* @details Přegeneruje všechny barvy v grafu tak, aby byly co nejvíce
	*          rozlišitelné
	*/
	void resetColors();

	/**
	* @brief Aktualizuje seznam otevřených souborů, graf a tabulku.
	*/
	void updateViews();
};

#endif
