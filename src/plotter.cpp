/**
* @file plotter.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief hlavní okno aplikace
*/
#include <QtGui>
#include <QtAlgorithms>
#include <fstream>
#include <iostream>
#include "plotter.h"
#include "plottertoolbar.h"
#include "plotterchart.h"
#include "plottertable.h"
#include "plotterfileslist.h"
#include "dataimport.h"
#include "color.h"
#include "plottersettings.h"

using namespace std;


/**
* @brief Vytvoří hlavní okno programu s celým jeho obsahem.
* @param parent rodičovský widget
*/
Plotter::Plotter(QWidget *parent) : QWidget(parent)
{
	// hlavni vertikalni layout
	QVBoxLayout *layout = new QVBoxLayout;
	layout->setSpacing(0);
	layout->setContentsMargins(0, 0, 0, 0);

	// nastrojova lista
	toolbar = new PlotterToolbar(this);
	toolbar->setRoot(this);
	layout->addWidget(toolbar);

	// oddelovac
	QSplitter *splitter = new QSplitter(Qt::Horizontal);
	splitter->setChildrenCollapsible(false);
	layout->addWidget(splitter);

	// panel otevrenych souboru
	openedFiles = new PlotterFilesList(this);
	openedFiles->setRoot(this);
	splitter->addWidget(openedFiles);

	// pravy panel
	QWidget *rightCont = new QWidget(splitter);
	rightCont->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	QHBoxLayout *rightLayout = new QHBoxLayout;
	rightLayout->setSpacing(0);
	rightLayout->setContentsMargins(0, 0, 0, 0);
	rightCont->setLayout(rightLayout);
	rightCont->setMinimumWidth(150);

	// graf a tabulka
	chart = new PlotterChart(this);
	chart->setRoot(this);
	rightLayout->addWidget(chart, 1);
	table = new PlotterTable(this);
	table->setRoot(this);
	rightLayout->addWidget(table, 1);
	table->hide();

	// nastavit sirky leveho sloupce a grafu
	QList<int> sizes;
	sizes << 10 << 330;
	splitter->setSizes(sizes);

	// atributy hlavniho okna
	setWindowTitle("Plotter");
	setMinimumWidth(800);
	setMinimumHeight(500);
	setLayout(layout);
	centerWindow();
	applyStyle();
	isChartVisible = true;

	// pripojit udalosti
	connect(toolbar->openButton, SIGNAL(clicked()), this, SLOT(openFile()));
	connect(toolbar->saveButton, SIGNAL(clicked()), this, SLOT(saveChart()));
	connect(toolbar->zoomInBtn, SIGNAL(clicked()), chart, SLOT(zoomIn()));
	connect(toolbar->zoomOutBtn, SIGNAL(clicked()), chart, SLOT(zoomOut()));
	connect(toolbar->zoom100Btn, SIGNAL(clicked()), chart, SLOT(zoom100()));

	//nahrajeme nastaveni ze souboru
	PlotterSettings * settings = new PlotterSettings(this);
	settings->loadSettings((QApplication::applicationDirPath() + "/../examples/settings.xml").toStdString());
	delete settings;
	updateViews();

	
}


/**
* @brief Událost vyvolaná při ukončení aplikace. Uloží nastavení.
* @param event objekt události
*/
void Plotter::closeEvent(QCloseEvent *event)
{
	//nahrajeme nastaveni ze souboru
	PlotterSettings * settings = new PlotterSettings(this);
	settings->saveSettings((QApplication::applicationDirPath() + "/../examples/settings.xml").toStdString());
	delete settings;
}


/**
* @brief Přesune okno aplikace na střed obrazovky.
*/
void Plotter::centerWindow()
{
	QDesktopWidget *d = QApplication::desktop();
	QSize size = this->size();

	this->move((d->width() - size.width()) / 2, (d->height() - size.height()) / 2);
}


/**
* @brief Nastaví vzhled aplikace podle CSS souboru.
*/
void Plotter::applyStyle()
{
	QFile css(":/media/style.css");
	css.open(QIODevice::ReadOnly);

	if (not css.isOpen())
		return;

	setStyleSheet(css.readAll());
	css.close();
}


/**
* @brief Zobrazí dialog pro otevření souboru a přidá ho.
*/
void Plotter::openFile()
{
	PlotterDataset *dataset;
	string filename = QFileDialog::getOpenFileName(
		this,
		tr("Přidat datový soubor"),
		QDir::homePath(),
		"*.txt *.csv *.xml"
	).toStdString();

	// stisknuto Cancel nebo soubor uz je v seznamu
	if (filename.empty() or filenames.contains(filename))
		return;

	// nacist data ze souboru
	try {
		dataset = DataImport(filename).getData();
	}
	catch (Exception e) {
		QMessageBox::warning(this, "Chyba!", QString(e.msg().c_str()));
		return;
	}

	filenames.push_back(filename);
	dataset->initialize();
	datas.push_back(dataset);
	resetColors();
	updateViews();
}


/**
* @brief Zobrazí dialog pro uložení a uloží obrázek grafu.
*/
void Plotter::saveChart()
{
	string filename = QFileDialog::getSaveFileName(
		this,
		tr("Uložit graf"),
		QDir::homePath(),
		"*.png *.gif *.jpg"
	).toStdString();

	// stisknuto Cancel
	if (filename.empty())
		return;

	QImage img(chart->width(), chart->height(), QImage::Format_ARGB32_Premultiplied);
	QPainter painter(&img);
	chart->getScene()->render(&painter);
	painter.end();
	if (not img.save(filename.c_str()))
		QMessageBox::warning(this, "Chyba!", tr("Chyba při ukládání souboru!"));
}


/**
* @brief Znovunastavit barvy čar.
* @details Přegeneruje všechny barvy v grafu tak, aby byly co nejvíce
*          rozlišitelné
*/
void Plotter::resetColors()
{
	QVector<PlotterDataset *>::iterator ds;
	int idx = 0;
	int count;

	// vsechny datasety
	for (ds = datas.begin(); ds < datas.end(); ds++) {
		count = (*ds)->labels.size() - 1;
		(*ds)->colors.clear();

		// vsechny sloupce v datasetu
		for (int i = 0; i < count; i++) {
			Color col;
			col.setFromIndex(idx);
			(*ds)->colors.push_back(col);
			idx++;
		}
	}
}


/**
* @brief Aktualizuje seznam otevřených souborů, graf a tabulku.
*/
void Plotter::updateViews()
{
	openedFiles->update();
	chart->update();
	table->update();
}


/**
* @brief Změní barvu čáry v grafu.
* @param ds_idx index datasetu
* @param column_idx index datového sloupce
* @param color barva
*/
void Plotter::setColor(int ds_idx, int column_idx, QColor color)
{
	QPen pen = datas[ds_idx]->lines[column_idx]->pen();
	pen.setColor(color);
	
	datas[ds_idx]->lines[column_idx]->setPen(pen);
	datas[ds_idx]->colors[column_idx] = color;
}


/**
* @brief Změní styl čáry v grafu.
* @param ds_idx index datasetu
* @param column_idx index datového sloupce
* @param style styl čáry
*/
void Plotter::setLineStyle(int ds_idx, int column_idx, Qt::PenStyle style)
{
	QPen pen = datas[ds_idx]->lines[column_idx]->pen();
	pen.setStyle(style);
	
	datas[ds_idx]->lines[column_idx]->setPen(pen);
	datas[ds_idx]->styles[column_idx] = style;
}


/**
* @brief Zobrazí datový sloupec a překreslí pohledy.
* @param ds_idx index datasetu
* @param column_idx index datového sloupce
*/
void Plotter::showColumn(int ds_idx, int column_idx)
{
	datas[ds_idx]->visibles[column_idx] = true;

	if (isChartVisible) {
		chart->show();
		table->hide();
		chart->update();
	}
	else {
		chart->hide();
		table->show();
		table->update();
	}
}


/**
* @brief Schová datový sloupec a překreslí pohledy.
* @param ds_idx index datasetu
* @param column_idx index datového sloupce
*/
void Plotter::hideColumn(int ds_idx, int column_idx)
{
	datas[ds_idx]->visibles[column_idx] = false;

	if (isChartVisible) {
		chart->show();
		table->hide();
		chart->update();
	}
	else {
		chart->hide();
		table->show();
		table->update();
	}
}


/**
* @brief Odstraní dataset z paměti a obnoví pohledy.
* @param ds_idx index datasetu
*/
void Plotter::removeDataset(int ds_idx)
{
	filenames.erase(filenames.begin() + ds_idx);
	datas.erase(datas.begin() + ds_idx);
	updateViews();
}


