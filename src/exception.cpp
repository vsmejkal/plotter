/**
* @file exception.cpp
* @author Vojtěch Smejkal (xsmejk07) , Tomáš Sychra (xsychr03)
* @date 6.5.2010
* @brief Modul Exception, obsluha výjimek
* @details ICP - implementace tříd výjimek
*/
#include <string>
#include <iostream>
#include <exception>
#include "exception.h"


/**
* @details Konstruktor výjimek.
*/
Exception::Exception(string msg)
{
	this->str_exception=msg;
}

