# Plotter: Visualization software

## Description
App reads data files and plots them onto chart or displays it in table.

Top panel contains buttons for adding new data files, saving plot to image,
and modifying various plot parameters. Section on the right allows to switch
between plot-view and table-view and also to zoom the view. Left part contains
a list of opened files with corresponding data columns. It is possible to show
or hide any column.

## Features
- Set line color and style
- Change data range by dragging left mouse button
- Linear / logarithmic scale
- Save plot to image
- Turn on/off the line visibility

## Requirements
- GCC compiler
- Qt Toolkit 4.5.0
- OpenGL

## Authors
Vojtěch Smejkal, xsmejk07@stud.fit.vutbr.cz
(User interface, Plot widget, Table view)

Tomáš Sychra, xsychr03@stud.fit.vutbr.cz
(XML import/export)

FIT BUT Brno, 2010
